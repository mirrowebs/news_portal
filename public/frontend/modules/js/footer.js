$(document).ready(function () {
    $('#saveSubscribers').validate({
        ignore: [],
        errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        // errorPlacement: function (error, e) {
        //     e.parents('.form-group').append(error);
        // },
        // highlight: function (e) {
        //     $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
        //     $(e).closest('.help-block').remove();
        // },
        // success: function (e) {
        //     // You can use the following if you would like to highlight with green color the input after successful validation!
        //     e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
        //     e.closest('.help-block').remove();
        // },
        rules: {
            s_email: {
                required: true,
                email: true
            }
        },
        messages: {
            s_email: {
                required: 'Please Email Field is Required'
            }
        },
        submitHandler: function (form) {
            $('.subscribersformStatus').html('');
           // var token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: "/ajax/save/subscribers",
                data: $(form).serialize(),
                success: function (response) {


                    // console.log(response.status)
                    //
                    // // if(response === 'object'){
                    // //     console.log('obj')
                    // // }else{
                    // //     console.log('nonobj')
                    // //     response = JSON.parse(response) ;
                    // // }

                    if (response.status == 1) {

                        $('#saveSubscribers')[0].reset();
                        $('.subscribersformStatus').html('<div class="alert alert-success">\n' +
                            '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                            '    <strong>Success!</strong> Your Email Subscribe Successfully.\n' +
                            '</div>');
                    } else {
                        $('.subscribersformStatus').html('<div class="alert alert-danger">\n' +
                            '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                            '    <strong>' + response.errors + '</strong>\n' +
                            '</div>');
                    }
                }
            });
            return false; // required to block normal submit since you used ajax
        }
    });

});

