$(document).ready(function () {
    $('.galleries_delete_gallery_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/galleries/deletgalleryImage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.galleries_postgimagediv' + image).remove();
                }
            });
        }
    });

    $('.galleries_post_title').on('input', function () {
        var post_title = $(this).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: '/admin/ajax/galleries/createcatalias',
            type: 'POST',
            data: {'post_title': post_title},
            success: function (response) {
                $('.galleries_post_alias').val(response)
            }
        });
    });

    $('.galleries_delete_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/galleries/deletepostImage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.galleries_postimagediv').remove();
                }
            });
        }
    });
    $('#galleries_validation').validate({
        ignore: [],
        errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.help-block').remove();
        },
        rules: {
            g_alias: {
                required: true
            },
            // p_meta_desc: {
            //     required: true
            // },
            // p_meta_keywords: {
            //     required: true
            // },
            g_title: {
                required: true
            }
        },
        messages: {
            g_alias: {
                required: 'Please enter alias'
            },
            // p_meta_desc: {
            //     required: 'Please enter meta descriptions'
            // },
            // p_meta_keywords: {
            //     required: 'Please enter meta keywords'
            // },
            g_title: {
                required: 'Please enter title'
            }
        },
    });

    $('.gallery_priority').each(function () {
        $(this).validate({
            ignore: [],
            errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',

            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
            },
            rules: {
                // pi_priority: {
                //     required: true,
                //     number: true
                // }
            },
            messages: {
                gi_priority: {
                    required: 'Please enter number',
                    number: 'Enter numbers only'
                }
            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: "/admin/ajax/gallery/save/imagepriority",
                    data: $(form).serialize(),
                    success: function (response) {
                        $('.galleryimages').html('');
                        if (response.status == 1) {
                            $('.galimgstatus' + response.requestedid).html('<div class="alert alert-success">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>Success!</strong> Your message has been sent successfully.\n' +
                                '</div>');
                        } else {
                            $('.galimgstatus' + response.requestedid).html('<div class="alert alert-danger">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>' + response.errors + '</strong>\n' +
                                '</div>');
                        }
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        })
    });
});
