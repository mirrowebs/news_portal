$(document).ready(function () {
    $('.events_delete_gallery_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/events/deletgalleryImage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.events_postgimagediv' + image).remove();
                }
            });
        }
    });

    $('.events_post_title').on('input', function () {
        var post_title = $(this).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: '/admin/ajax/events/createcatalias',
            type: 'POST',
            data: {'post_title': post_title},
            success: function (response) {
                $('.events_post_alias').val(response)
            }
        });
    });

    $('.events_delete_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/events/deletepostImage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.events_postimagediv').remove();
                }
            });
        }
    });
    $('#events_validation').validate({
        ignore: [],
        errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.help-block').remove();
        },
        rules: {
            e_alias: {
                required: true
            },
            // p_meta_desc: {
            //     required: true
            // },
            // p_meta_keywords: {
            //     required: true
            // },
            // p_schedule_info: {
            //     required: true
            // },
            e_title: {
                required: true
            }
        },
        messages: {
            e_alias: {
                required: 'Please enter alias'
            },
            // p_meta_desc: {
            //     required: 'Please enter meta descriptions'
            // },
            // p_meta_keywords: {
            //     required: 'Please enter meta keywords'
            // },
            // p_schedule_info: {
            //     required: 'Please enter schedule info'
            // },
            e_title: {
                required: 'Please enter title'
            }
        },
    });
    $('.gallery_priority').each(function () {
        $(this).validate({
            ignore: [],
            errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',

            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
            },
            rules: {
                // pi_priority: {
                //     required: true,
                //     number: true
                // }
            },
            messages: {
                ei_priority: {
                    required: 'Please enter number',
                    number: 'Enter numbers only'
                }
            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: "/admin/ajax/events/save/imagepriority",
                    data: $(form).serialize(),
                    success: function (response) {
                        $('.galleryimages').html('');
                        if (response.status == 1) {
                            $('.galimgstatus' + response.requestedid).html('<div class="alert alert-success">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>Success!</strong> Your message has been sent successfully.\n' +
                                '</div>');
                        } else {
                            $('.galimgstatus' + response.requestedid).html('<div class="alert alert-danger">\n' +
                                '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                '    <strong>' + response.errors + '</strong>\n' +
                                '</div>');
                        }
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        })
    });
});
