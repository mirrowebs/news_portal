$(document).ready(function () {
    $('#contact_validation').validate({
        ignore: [],
        errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.help-block').remove();
        },
        rules: {
            cs_name: {
                required: true
            },
            cs_email: {
                required: true,
                email: true
            },
            cs_message: {
                required: true
            },
            cs_contact_number: {
                required: true,
                number: true
            }
        },
        messages: {
            cs_name: {
                required: 'Please enter name'
            },
            cs_email: {
                required: 'Please enter Email',
                email: "Enter valid email"
            },
            cs_message: {
                required: 'Please enter message'
            },
            cs_contact_number: {
                required: 'Please Enter Contact n   umber',
                number: 'Please Enter Numbers Only'
            }
        },
    });
});
