$(document).ready(function () {
    $('#webseries_validation').validate({
        ignore: [],
        errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.help-block').remove();
        },
        rules: {
            w_title: {
                required: true
            },
            w_logo: {
                required: true
            },
            w_banner: {
                required: true
            },
            w_link: {
                required: true
            },
            w_description: {
                required: true
            }
        },
        messages: {
            w_title: {
                required: 'Please Enter Webseries title'
            },
            w_logo: {
                required: 'Please Upload Webseries Logo'
            },
            w_banner: {
                required: 'Please Upload Webseries Banner'
            },
            w_link: {
                required: 'Please Enter Webseries link'
            },
            w_description: {
                required: 'Please Enter Webseries Description'
            }
        },
    });
});
