$(document).ready(function () {
    $('#settings_validation').validate({
        ignore: [],
        errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.help-block').remove();
        },
        rules: {
            s_mobile: {
                required: true
            },

            s_email: {
                required: true
            },
            s_address: {
                required: true
            },
            s_facebook: {
                required: true
            }
        },
        messages: {
            s_mobile: {
                required: 'Please enter mobile number'
            },

            s_email: {
                required: 'Please enter email'
            },
            s_address: {
                required: 'Please enter address'
            },
            s_facebook: {
                required: 'Please enter facebook link'
            }
        },
    });
});
