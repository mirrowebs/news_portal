$(document).ready(function () {


    loadResourcesByResourcesType('.tagsinput', '.post_videos_frame');

    function loadResourcesByResourcesType(inputclass, frameclass){

        var postCurrentTags = $(inputclass).val();


        var tagslits = postCurrentTags.split(',');




        tagslits = tagslits.filter(Boolean);
        // console.log(tagslits);
        if (tagslits.length > 0) {
            $.each(tagslits, function (index, item) {

                var videoid = '';

                if (item.indexOf(':') !== -1) {
                    var itds =  item.split(":");
                    // item = item[0];
                    // console.log(item);
                    console.log(itds[0]);
                    videoid = itds[1];
                }else{
                    videoid = item;

                }



                $(frameclass).append('<div class="' + videoid + ' col-md-2"><img class="" style="margin: 5px" src="https://img.youtube.com/vi/' + videoid + '/default.jpg" alt="Question" /></div>')
            })
        }

        $(inputclass).tagsInput({
            width: 'auto',
            placeholder: 'title:videoid, videoid',
            'onAddTag': function (item) {

                var videoid = '';
                if (item.indexOf(':') !== -1) {
                    var itds =  item.split(":");
                    // item = item[0];
                    // console.log(item);
                    console.log(itds[0]);
                    videoid = itds[1];
                }else{
                    videoid = item;

                }



                $(frameclass).append('<div class="' + videoid + ' col-md-2"><img class="" style="margin: 5px" src="https://img.youtube.com/vi/' + videoid + '/default.jpg" alt="Question" /></div>')
            },
            'onRemoveTag': function (item) {
                $(frameclass + ' .' + item).remove();
            }
        });


    }

    $('#videos_validation').validate({
        ignore: [],
        errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.help-block').remove();
        },
        rules: {
            p_alias: {
                required: true
            },
            // p_meta_desc: {
            //     required: true
            // },
            // p_meta_keywords: {
            //     required: true
            // },
            p_title: {
                required: true
            }
        },
        messages: {
            p_alias: {
                required: 'Please enter alias'
            },
            // p_meta_desc: {
            //     required: 'Please enter meta descriptions'
            // },
            // p_meta_keywords: {
            //     required: 'Please enter meta keywords'
            // },
            p_title: {
                required: 'Please enter title'
            }
        },
    });
});
