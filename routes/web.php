<?php

Route::get('/', 'frontend\HomeController@index')->name('home');


Auth::routes();

Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/changePassword', 'Admin\HomeController@showChangePasswordForm')->name('changePassword');
    Route::post('/changePassword', 'Admin\HomeController@showChangePasswordForm')->name('changePassword');
    Route::get('/dashboard', 'Admin\HomeController@index')->name('dashboard');
//  Route::resource('/settings', 'Admin\\settingsController');

    Route::resource('/settings', 'Admin\\settingsController', [
        'names' => [
            'index' => 'manageSettings',
            'create' => 'addSettings',
            'show' => 'viewSettings',
            'edit' => 'editSettings',
    ],
    ]);

    Route::resource('/categories', 'Admin\\CategoriesController', [
        'names' => [
            'index' => 'manageCategories',
            'create' => 'addCategories',
            'show' => 'viewCategories',
            'edit' => 'editCategories',
            'delete' => 'deleteCategories',
        ],
    ]);


});

