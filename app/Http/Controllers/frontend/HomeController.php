<?php

namespace App\Http\Controllers\frontend;


use App\Models\Apps;
use App\Models\Entertinement;
use App\Models\Events;
use App\Models\EventsImages;
use App\Models\Gallery;
use App\Models\GalleryImages;
use App\Models\Interviews;
use App\Models\ContactSubmission;
use App\Models\setting;
use App\Models\Subscribers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Blog;
use App\Models\Webseries;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Mail\ContactMail;




class HomeController extends Controller
{
    public function index()
    {
        $data = array();
//        $banners = Banner::where('ba_status', 1)->get();
//        $data['banners'] = $banners;
//        $settings = setting::first();
//        $data['settings'] = $settings;
        $data['title'] = 'Home | Idreammedia.com';
        $data['active'] = 'home';
        $data['sub_active'] = 'home';
        return view('frontend.home', $data);
    }

    public function aboutus()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'About Us | IdreamMedia.com';
        $data['active'] = 'aboutus';
        $data['sub_active'] = 'aboutus';
        return view('frontend.aboutus', $data);
    }

    public function blog()
    {
        $data = array();
        $blog = Blog::where('b_status', 1)->get();
        $data['blog'] = $blog;
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Blog | IdreamMedia.com';
        $data['active'] = 'blog';
        $data['sub_active'] = 'blog';
        return view('frontend.blog', $data);
    }


    public function blogDetails($id)
    {
        $data = array();

        $settings = setting::first();
        $data['settings'] = $settings;

        $blog = Blog::findOrFail($id);
        $data['blog'] = $blog;

        $blogs = Blog::where('b_status', 1)->orderBy('b_id', 'desc')->take(5)->get();
        $data['blogs'] = $blogs;

        $data['title'] = 'Blog | '.$blog->b_title. ' | IdreamMedia.com';
        $data['active'] = 'blogs';
        $data['sub_active'] = 'blogs';
        return view('frontend.blog_details', $data);
    }

    public function privacypolicy()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Privacy Policy | IdreamMedia.com';
        $data['active'] = 'privacypolicy';
        $data['sub_active'] = 'privacypolicy';
        return view('frontend.privacy-policy', $data);
    }

    public function termsconditions()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Terms & Conditions | IdreamMedia.com';
        $data['active'] = 'termsconditions';
        $data['sub_active'] = 'termsconditions';
        return view('frontend.terms-conditions', $data);
    }

    public function WebSeries()
    {
        $data = array();
        $webseries = Webseries::where('w_status', 1)->get();
        $data['webseries'] = $webseries;
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'WebSeries | IdreamMedia.com';
        $data['active'] = 'webseries';
        $data['sub_active'] = 'webseries';
        return view('frontend.OriginalContent.webseries', $data);
    }

    public function Entertainments()
    {
        $data = array();
        $entertainments = Entertinement::where('ent_status', 1)->get();
        $data['entertainments'] = $entertainments;
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Entertinement Series | IdreamMedia.com';
        $data['active'] = 'entertainments';
        $data['sub_active'] = 'entertainments';
        return view('frontend.OriginalContent.entertainments', $data);
    }

    public function ClientChannels()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Partner Channels | IdreamMedia.com';
        $data['active'] = 'partner';
        $data['sub_active'] = 'partner';
        return view('frontend.OriginalContent.partner-channels', $data);
    }


    public function Interviews()
    {
        $data = array();
        $interviews = Interviews::where('cat_status', 1)->get();
        $data['interviews'] = $interviews;
        $data['interviews2'] = $interviews;
        $data['interviews3'] = $interviews;
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Interviews | IdreamMedia.com';
        $data['active'] = 'interviews';
        $data['sub_active'] = 'interviews';
        return view('frontend.interviews', $data);
    }


    public function Events()
    {
        $data = array();
        $events = Events::where('e_status', 1)->orderBy('e_id', 'desc')->get();
        $data['events'] = $events;
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Events | IdreamMedia.com';
        $data['active'] = 'events';
        $data['sub_active'] = 'events';
        return view('frontend.events', $data);
    }

    public function eventDetails($id)
    {
        $data = array();

        $settings = setting::first();
        $data['settings'] = $settings;

        $event = Events::findOrFail($id);
        $data['event'] = $event;

        $gallery = EventsImages::where('ei_e_id', $id)->orderBy('ei_priority', 'desc')->get();
        $data['gallery'] = $gallery;

        $data['title'] = 'Events | '.$event->e_title. ' | IdreamMedia.com';
        $data['active'] = 'events';
        $data['sub_active'] = 'events';
        return view('frontend.event_details', $data);
    }



    public function Gallery()
    {
        $data = array();

        $galleries = Gallery::where('g_status', 1)->orderBy('g_id', 'desc')->with('galleryImages')->get();
        $data['galleries'] = $galleries;
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Gallery | IdreamMedia.com';
        $data['active'] = 'gallery';
        $data['sub_active'] = 'gallery';
        return view('frontend.gallery', $data);
    }

    public function galleryDetails($id)
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;

        $galleryInfo = Gallery::findOrFail($id);
        $data['galleryInfo'] = $galleryInfo;

        $gallery = GalleryImages::where('gi_g_id', $id)->orderBy('gi_priority', 'asc')->get();
        $data['gallery'] = $gallery;

        $data['title'] = 'Gallery | '.$galleryInfo->g_title. ' | IdreamMedia.com';
        $data['active'] = 'gallery';
        $data['sub_active'] = 'gallery';
        return view('frontend.gallery_details', $data);
    }

    public function DigitalMarketing()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Digital Marketing | IdreamMedia.com';
        $data['active'] = 'digitalmarketing';
        $data['sub_active'] = 'digitalmarketing';
        return view('frontend.digital-marketing', $data);
    }

    public function SocialMediaManagement()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Social Media Management | IdreamMedia.com';
        $data['active'] = 'socialmedia';
        $data['sub_active'] = 'socialmedia';
        return view('frontend.socialmedia-management', $data);
    }

    public function saveSocialMediaSubmissions(Request $request)
    {

        $requestData = $request->all();

        $this->validate(request(), [
            'cs_name' => 'required',
            'cs_contact_number' => 'required',
            'cs_email' => 'required|email',
            'cs_website' => 'required',
            'cs_company_name' => 'required',
            'cs_location' => 'required',
            'cs_message' => 'required'
        ], [
            'cs_name.required' => 'Name Field is Required',
            'cs_contact_number.required' => 'Contact Number Field is Required',
            'cs_email.required' => 'Email Field is Required',
            'cs_email.email' => 'Email must be a valid email address',
            'cs_website.required' => 'Website Field is Required',
            'cs_company_name.required' => 'Company Name Field is Required',
            'cs_location.required' => 'Location Field is Required',
            'cs_message.required' => 'Message Field is Required'
        ]);


        $requestData['cs_type'] = 'SocialMedia';

        ContactSubmission::create($requestData);



        $email=$requestData['cs_email'];
        $name=$requestData['cs_name'];

        Mail::send('frontend.emails.socialmedia.socialmediamail', ['name' => $name,'email' => $email], function ($message) use ($request)
        {
            $message->from('admin@idreammedia.com', 'Idream Media');
//            $message->to( $request->input('email') );
            $message->to( $request->input('cs_email'));
            //Add a subject
            $message->subject("Thank you for contacting us!");
        });




        $name=$requestData['cs_name'];
        $phone=$requestData['cs_contact_number'];
        $email=$requestData['cs_email'];
        $text=$requestData['cs_message'];
        $website=$requestData['cs_website'];
        $companyname=$requestData['cs_company_name'];
        $location=$requestData['cs_location'];
        $emails = ['rajagonda@gmail.com', 'sai96030@gmail.com'];
        Mail::send('frontend.emails.socialmedia.socialmediamailtoadmin', ['name' => $name, 'email' => $email, 'phone' => $phone, 'text' => $text, 'website' => $website, 'companyname' => $companyname, 'location' => $location], function ($message) use ($request, $emails)
        {
            $message->from($request->input('cs_email'), $request->input('cs_name'));
//            $message->to( $request->input('email') );
            $message->to( $emails);
            //Add a subject
            $message->subject("Contact Form Message From ". $request->input('cs_name'));
        });





        return redirect('services/socialmedia-management')->with('flash_message', 'Thank you for Your Submission, We will contact you soon');
    }


    public function SoundDesign()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Sound Design | IdreamMedia.com';
        $data['active'] = 'aboutus';
        $data['sub_active'] = 'aboutus';
        return view('frontend.sound-design', $data);
    }

    public function Editing()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Editing | IdreamMedia.com';
        $data['active'] = 'editing';
        $data['sub_active'] = 'editing';
        return view('frontend.editing', $data);
    }

    public function saveEditingSubmissions(Request $request)
    {

        $requestData = $request->all();

        $this->validate(request(), [
            'cs_name' => 'required',
            'cs_contact_number' => 'required',
            'cs_email' => 'required|email',
            'cs_website' => 'required',
            'cs_company_name' => 'required',
            'cs_location' => 'required',
            'cs_message' => 'required'
        ], [
            'cs_name.required' => 'Name Field is Required',
            'cs_contact_number.required' => 'Contact Number Field is Required',
            'cs_email.required' => 'Email Field is Required',
            'cs_email.email' => 'Email must be a valid email address',
            'cs_website.required' => 'Website Field is Required',
            'cs_company_name.required' => 'Company Name Field is Required',
            'cs_location.required' => 'Location Field is Required',
            'cs_message.required' => 'Message Field is Required'
        ]);


        $requestData['cs_type'] = 'Editing';

        ContactSubmission::create($requestData);


        $email=$requestData['cs_email'];
        $name=$requestData['cs_name'];

        Mail::send('frontend.emails.editing.editingmail', ['name' => $name,'email' => $email], function ($message) use ($request)
        {
            $message->from('admin@idreammedia.com', 'Idream Media');
//            $message->to( $request->input('email') );
            $message->to( $request->input('cs_email'));
            //Add a subject
            $message->subject("Thank you for contacting us!");
        });




        $name=$requestData['cs_name'];
        $phone=$requestData['cs_contact_number'];
        $email=$requestData['cs_email'];
        $text=$requestData['cs_message'];
        $website=$requestData['cs_website'];
        $companyname=$requestData['cs_company_name'];
        $location=$requestData['cs_location'];
        $emails = ['rajagonda@gmail.com', 'sai96030@gmail.com'];
        Mail::send('frontend.emails.editing.editingmailtoadmin', ['name' => $name, 'email' => $email, 'phone' => $phone, 'text' => $text, 'website' => $website, 'companyname' => $companyname, 'location' => $location], function ($message) use ($request, $emails)
        {
            $message->from($request->input('cs_email'), $request->input('cs_name'));
//            $message->to( $request->input('email') );
            $message->to( $emails);
            //Add a subject
            $message->subject("Contact Form Message From ". $request->input('cs_name'));
        });



        return redirect('services/film-video-editing')->with('flash_message', 'Thank you for Your Submission, We will contact you soon');
    }

    public function ProductionService()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Production Service| IdreamMedia.com';
        $data['active'] = 'production';
        $data['sub_active'] = 'production';
        return view('frontend.production-service', $data);
    }


    public function saveProductionSubmissions(Request $request)
    {

        $requestData = $request->all();

        $this->validate(request(), [
            'cs_name' => 'required',
            'cs_contact_number' => 'required',
            'cs_email' => 'required|email',
            'cs_website' => 'required',
            'cs_company_name' => 'required',
            'cs_location' => 'required',
            'cs_message' => 'required'
        ], [
            'cs_name.required' => 'Name Field is Required',
            'cs_contact_number.required' => 'Contact Number Field is Required',
            'cs_email.required' => 'Email Field is Required',
            'cs_email.email' => 'Email must be a valid email address',
            'cs_website.required' => 'Website Field is Required',
            'cs_company_name.required' => 'Company Name Field is Required',
            'cs_location.required' => 'Location Field is Required',
            'cs_message.required' => 'Message Field is Required'
        ]);


        $requestData['cs_type'] = 'ProductionService';

        ContactSubmission::create($requestData);


        $email=$requestData['cs_email'];
        $name=$requestData['cs_name'];

        Mail::send('frontend.emails.productionservice.productionmail', ['name' => $name,'email' => $email], function ($message) use ($request)
        {
            $message->from('admin@idreammedia.com', 'Idream Media');
//            $message->to( $request->input('email') );
            $message->to( $request->input('cs_email'));
            //Add a subject
            $message->subject("Thank you for contacting us!");
        });




        $name=$requestData['cs_name'];
        $phone=$requestData['cs_contact_number'];
        $email=$requestData['cs_email'];
        $text=$requestData['cs_message'];
        $website=$requestData['cs_website'];
        $companyname=$requestData['cs_company_name'];
        $location=$requestData['cs_location'];
        $emails = ['rajagonda@gmail.com', 'sai96030@gmail.com'];
        Mail::send('frontend.emails.productionservice.productionmailtoadmin', ['name' => $name, 'email' => $email, 'phone' => $phone, 'text' => $text, 'website' => $website, 'companyname' => $companyname, 'location' => $location], function ($message) use ($request, $emails)
        {
            $message->from($request->input('cs_email'), $request->input('cs_name'));
//            $message->to( $request->input('email') );
            $message->to( $emails);
            //Add a subject
            $message->subject("Contact Form Message From ". $request->input('cs_name'));
        });



        return redirect('services/production-service')->with('flash_message', 'Thank you for Your Submission, We will contact you soon');
    }







    public function Idreamcampus()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'IdreamCampus | IdreamMedia.com';
        $data['active'] = 'idreamcampus';
        $data['sub_active'] = 'idreamcampus';
        return view('frontend.idreamcampus', $data);
    }

    public function Idreampost()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'IdreamPost | IdreamMedia.com';
        $data['active'] = 'idreampost';
        $data['sub_active'] = 'idreampost';
        return view('frontend.idreampost', $data);
    }

    public function Ourapps()
    {
        $data = array();
        $apps = Apps::where('app_status', 1)->get();
        $data['apps'] = $apps;
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Our Apps | IdreamMedia.com';
        $data['active'] = 'ourapps';
        $data['sub_active'] = 'ourapps';
        return view('frontend.our-apps', $data);
    }

    public function Contactus()
    {
        $data = array();
        $settings = setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'Contact Us | IdreamMedia.com';
        $data['active'] = 'contact';
        $data['sub_active'] = 'contact';
        return view('frontend.contactus', $data);
    }


    public function saveContactSubmissions(Request $request)
    {

        $requestData = $request->all();

        $this->validate(request(), [
            'cs_name' => 'required',
            'cs_contact_number' => 'required',
            'cs_email' => 'required|email',
            'cs_message' => 'required'
        ], [
            'cs_name.required' => 'Name Field is Required',
            'cs_contact_number.required' => 'Contact Number Field is Required',
            'cs_email.required' => 'Email Field is Required',
            'cs_email.email' => 'Email must be a valid email address',
            'cs_message.required' => 'Message Field is Required'
        ]);


        $requestData['cs_type'] = 'Contact';

        ContactSubmission::create($requestData);

//        $data = array(
//            'subject'=> 'Thank you for contacting us!',
//            'request_data'=> $requestData,
//            'message'=> 'Thank you for Your Submission, We will contact you soon'
//        );

        //Mail::to($data['request_data']['cs_email'])->send(new ContactMail($data));

        $email=$requestData['cs_email'];
        $name=$requestData['cs_name'];
        Mail::send('frontend.emails.contactform.contactusmail', ['name' => $name,'email' => $email], function ($message) use ($request)
        {
            $message->from('info@idreammedia.com', 'Idream Media');
//            $message->to( $request->input('email') );
            $message->to( $request->input('cs_email'));
            //Add a subject
            $message->subject("Thank you for contacting us!");
        });



        $name=$requestData['cs_name'];
        $phone=$requestData['cs_contact_number'];
        $email=$requestData['cs_email'];
        $text=$requestData['cs_message'];
       $emails = ['rajagonda@gmail.com', 'sai96030@gmail.com'];
        //$emails = ['sai96030@gmail.com'];
        Mail::send('frontend.emails.contactform.contactusmailtoadmin', ['name' => $name, 'email' => $email, 'phone' => $phone, 'text' => $text], function ($message) use ($request, $emails)
        {
            $message->from($request->input('cs_email'), $request->input('cs_name'));
//            $message->to( $request->input('email') );
            $message->to( $emails);
            //Add a subject
            $message->subject("Contact Form Message From ". $request->input('cs_name'));
        });





        return redirect('contactus')->with('flash_message', 'Thank you for Your Submission, We will contact you soon');
    }

    public function saveSubscribers(Request $request)
    {
        $return = array();
        $requestData = $request->all();

        $validator = Validator::make($requestData,
            [
                's_email' => 'unique:subscribers,s_email|required|email'
            ],
            [
                's_email.required' => 'Please enter email',
                's_email.email' => 'Please enter valid email',
                's_email.unique' => 'This Email Already Exist'

            ]
        );


        if ($validator->fails()) {
            $return['status'] = 2;
            $test = array();
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                array_push($test, $messages[0]);// messages are retrieved (publicly)
            }
            $return['errors'] = implode('<br>', $test);
        } else {
            $cdetails = Subscribers::create($requestData);
            $return['status'] = 1;
            $email=$requestData['s_email'];
            Mail::send('frontend.emails.subscribers.tosubscribersmail', ['email' => $email], function ($message) use ($request)
            {
                $message->from('info@idreammedia.com', 'Idream Media');
//            $message->to( $request->input('email') );
                $message->to( $request->input('s_email'));
                //Add a subject
                $message->subject("Thank you for Subscribing to IdreamMedia!");
            });
        }

        return $return;

    }






}
