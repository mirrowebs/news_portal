<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\setting;
use Illuminate\Http\Request;
use File;

class settingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $settings = setting::all();
        $data = array();
        $data['title'] = 'Settings';
        $data['settings'] = $settings;
        $data['active'] = 'settings';
        $data['main_active'] = 'settingsmenu';
        return view('admin.settings.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data = array();
        $data['title'] = 'Settings';
        $data['active'] = 'settings';
        $data['main_active'] = 'settingsmenu';
        return view('admin.settings.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            's_logo' => 'required',
            's_address' => 'required',
            's_facebook' => 'required'
        ]);

        $requestData = $request->all();


        if ($request->hasFile('s_logo')) {
            foreach ($request['s_logo'] as $file) {
                $uploadPath = public_path('/uploads/settings');

                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['s_logo'] = $fileName;
            }
        }

        setting::create($requestData);

        return redirect('admin/settings')->with('flash_message', 'setting added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $setting = setting::findOrFail($id);
        $data = array();
        $data['title'] = 'Settings';
        $data['active'] = 'settings';
        $data['setting'] = $setting;
        $data['main_active'] = 'settingsmenu';
        return view('admin.settings.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $setting = setting::findOrFail($id);
        $data = array();
        $data['title'] = 'Settings';
        $data['active'] = 'settings';
        $data['setting'] = $setting;
        $data['main_active'] = 'settingsmenu';
        return view('admin.settings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            's_address' => 'required',
            's_facebook' => 'required'
        ]);

        $requestData = $request->all();

        $setting = setting::findOrFail($id);

        if ($request->hasFile('s_logo')) {
            File::delete('uploads/settings/' . $setting->s_logo);
            foreach ($request['s_logo'] as $file) {
                $uploadPath = public_path('/uploads/settings');

                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['s_logo'] = $fileName;
            }
        }

        $setting->update($requestData);

        return redirect('admin/settings')->with('flash_message', 'setting updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $setting = setting::findOrFail($id);
        File::delete('uploads/settings/' . $setting->s_logo);
        setting::destroy($id);
        return redirect('admin/settings')->with('flash_message', 'setting deleted!');
    }
}
