<?php

namespace App\Http\Controllers\Admin;

use Alaouy\Youtube\Facades\Youtube;
use App\Http\Requests;
use App\Models\Channels;
use App\Models\Videos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Rsdesponse
     */
    public function index()
    {
        $data = array();
        $data['active'] = 'dashboard';
        $data['title'] = 'dashboard';
        $data['main_active'] = 'dashboard';
        return view('admin.dashboard', $data);
    }







    public function showChangePasswordForm(Request $request)
    {

        if ($request->input('submit')) {

            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
            }
            if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
                //Current password and new password are same
                return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
            }
            $validatedData = $request->validate([
                'current-password' => 'required',
                'new-password' => 'required|string|min:6|confirmed',
            ]);
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            return redirect()->back()->with("success", "Password changed successfully !");
        }
        $data = array();
        $data['active'] = '';
        $data['title'] = 'Change Password';
        $data['main_active'] = '';
        return view('admin.auth.changepassword', $data);
    }
}
