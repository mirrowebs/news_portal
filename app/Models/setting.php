<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class setting extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 's_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['s_logo', 's_url', 's_mobile', 's_phone', 's_email', 's_address', 's_facebook', 's_twitter', 's_linkedin', 's_google_plus', 's_youtube_link'];


}
