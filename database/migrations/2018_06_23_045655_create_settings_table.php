<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('s_id');
            $table->string('s_logo');
            $table->string('s_url')->nullable();
            $table->string('s_mobile')->nullable();
            $table->string('s_phone')->nullable();
            $table->string('s_email')->nullable();
            $table->string('s_address')->nullable();
            $table->string('s_facebook')->nullable();
            $table->string('s_twitter')->nullable();
            $table->string('s_linkedin')->nullable();
            $table->string('s_google_plus')->nullable();
            $table->string('s_youtube_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
