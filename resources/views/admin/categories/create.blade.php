@extends('admin.layout')

@section('title', $title)


@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Add Category
                <small>Manage Categories</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{route('manageCategories')}}">Categories</a></li>
                <li class="active">Add Category</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-primary">
                        <form method="POST" id="categories_validation" action="{{ url('admin/Categories') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('admin.settings.form')

                        </form>
                    </div>

                </div>

            </div>
        </section>
    </div>


@endsection