@extends('admin.layout')

@section('title', $title)

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Update Category
                <small>Manage Categories</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{route('manageCategories')}}">Settings</a></li>
                <li class="active">Update Category</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-primary">
                        <br/>
                        <form method="POST" id="settings_validation" action="{{route('editCategories',['id' => $category->s_id])}}"
                              accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('admin.settings.form', ['submitButtonText' => 'Update'])

                        </form>

                    </div>

                </div>

            </div>
        </section>
    </div>


@endsection