
<div class="form-group {{ $errors->has('c_name') ? 'has-error' : ''}}">
    <label for="c_name" class="col-md-4 control-label">{{ 'Category Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="c_name" type="text" id="c_name"
               value="{{ (isset($categories->c_name)) ? $setting->c_name : old('c_name')}}">
        {!! $errors->first('c_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('c_alias') ? 'has-error' : ''}}">
    <label for="c_alias" class="col-md-4 control-label">{{ 'Alias' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="c_alias" type="text" id="c_alias"
               value="{{ (isset($setting->c_alias)) ? $setting->c_alias : old('c_alias')}}">
        {!! $errors->first('c_alias', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_phone') ? 'has-error' : ''}}">
    <label for="s_phone" class="col-md-4 control-label">{{ 'Phone' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_phone" type="text" id="s_phone"
               value="{{ (isset($setting->s_phone)) ? $setting->s_phone : old('s_phone')}}">
        {!! $errors->first('s_phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_email') ? 'has-error' : ''}}">
    <label for="s_email" class="col-md-4 control-label">{{ 'Email' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_email" type="text" id="s_email"
               value="{{ (isset($setting->s_email)) ? $setting->s_email : old('s_email')}}">
        {!! $errors->first('s_email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('s_address') ? 'has-error' : ''}}">
    <label for="s_address" class="col-md-4 control-label">{{ 'Address' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="s_address" type="textarea"
                  id="s_address">{{ (isset($setting->s_address)) ? $setting->s_address : old('s_address') }}</textarea>
        {!! $errors->first('s_address', '<p class="help-block">:message</p>') !!}
    </div>
</div>






<div class="form-group {{ $errors->has('s_facebook') ? 'has-error' : ''}}">
    <label for="s_facebook" class="col-md-4 control-label">{{ 'Facebook' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_facebook" type="text" id="s_facebook"
               value="{{ (isset($setting->s_facebook)) ? $setting->s_facebook : old('s_facebook')}}">
        {!! $errors->first('s_facebook', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_twitter') ? 'has-error' : ''}}">
    <label for="s_twitter" class="col-md-4 control-label">{{ 'Twitter' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_twitter" type="text" id="s_twitter"
               value="{{ (isset($setting->s_twitter)) ? $setting->s_twitter : old('s_twitter')}}">
        {!! $errors->first('s_twitter', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_youtube_link') ? 'has-error' : ''}}">
    <label for="s_youtube_link" class="col-md-4 control-label">{{ 'Youtube link' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_youtube_link" type="text" id="s_youtube_link"
               value="{{(isset($setting->s_youtube_link)) ? $setting->s_youtube_link : old('s_youtube_link')}}">
        {!! $errors->first('s_youtube_link', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_linkedin') ? 'has-error' : ''}}">
    <label for="s_linkedin" class="col-md-4 control-label">{{ 'Linkedin' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_linkedin" type="text" id="s_linkedin"
               value="{{ (isset($setting->s_linkedin)) ? $setting->s_linkedin : old('s_linkedin')}}">
        {!! $errors->first('s_linkedin', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_google_plus') ? 'has-error' : ''}}">
    <label for="s_google_plus" class="col-md-4 control-label">{{ 'Google Plus' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_google_plus" type="text" id="s_google_plus"
               value="{{ (isset($setting->s_google_plus)) ? $setting->s_google_plus : old('s_google_plus') }}">
        {!! $errors->first('s_google_plus', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
