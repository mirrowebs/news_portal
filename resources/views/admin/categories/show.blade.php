@extends('admin.layout')

@section('title', $title)

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Category View
                <small>Manage Categories</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('manageCategories')}}"><i class="fa fa-dashboard"></i>Categories</a></li>
                <li class="active">Settings view</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                <div class="box box-primary">
                <a href="{{route('manageCategories')}}" title="Back">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a>
                <a href="{{route('editCategories',['id' => $categories->c_id])}}" title="Edit setting">
                    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                    </button>
                </a>

                <form method="POST" action="{{route('deleteCategories',['id' => $item->c_id])}}" accept-charset="UTF-8"
                      style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-xs" title="Delete Category"
                            onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                     aria-hidden="true"></i> Delete
                    </button>
                </form>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                        <tr>
                            <th>ID</th>
                            <td>{{ $categories->c_id }}</td>
                        </tr>

                        <tr>
                            <th>Category Name</th>
                            <td> {{ $categories->c_name }} </td>
                        </tr>
                        <tr>
                            <th>Alias</th>
                            <td> {{ $categories->c_alias }} </td>
                        </tr>
                        <tr>
                            <th>Type</th>
                            <td> {{ $categories->s_type }} </td>
                        </tr>
                        <tr>
                            <th>Meta Title</th>
                            <td> {{ $categories->c_meta_title }} </td>
                        </tr>

                        <tr>
                            <th>Meta Description</th>
                            <td> {{ $categories->c_meta_description }} </td>
                        </tr>

                        <tr>
                            <th>Meta Keys</th>
                            <td> {{ $categories->c_meta_keys }} </td>
                        </tr>

                        <tr>
                            <th>Status</th>
                            <td> {{ $categories->c_status }} </td>
                        </tr>

                         </tbody>
                    </table>
                </div>

            </div>

            </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection