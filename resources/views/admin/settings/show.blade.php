@extends('admin.layout')

@section('title', $title)

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Settings view
                <small>Manage Settings</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{url('/admin/settings')}}"><i class="fa fa-dashboard"></i>Settings</a></li>
                <li class="active">Settings view</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                <div class="box box-primary">
                <a href="{{ url('/admin/settings') }}" title="Back">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a>
                <a href="{{ url('/admin/settings/' . $setting->s_id . '/edit') }}" title="Edit setting">
                    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                    </button>
                </a>

                {{--<form method="POST" action="{{ url('settings' . '/' . $setting->s_id) }}" accept-charset="UTF-8"--}}
                      {{--style="display:inline">--}}
                    {{--{{ method_field('DELETE') }}--}}
                    {{--{{ csrf_field() }}--}}
                    {{--<button type="submit" class="btn btn-danger btn-xs" title="Delete setting"--}}
                            {{--onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"--}}
                                                                                     {{--aria-hidden="true"></i> Delete--}}
                    {{--</button>--}}
                {{--</form>--}}
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                        <tr>
                            <th>ID</th>
                            <td>{{ $setting->s_id }}</td>
                        </tr>
                        <tr>
                            <th>Logo</th>
                            <td>  @php
                                    $image= ($setting->s_logo) ? '<img src="/uploads/settings/'.$setting->s_logo.'" width="200px" height="200px"/>' : '-';
                                @endphp

                                <a data-fancybox="" class="popupimages" href="{{url('/uploads/settings/'.$setting->s_logo)}}">
                                    {!! $image !!}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th>Mobile</th>
                            <td> {{ $setting->s_mobile }} </td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td> {{ $setting->s_phone }} </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td> {{ $setting->s_email }} </td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td> {{ $setting->s_address }} </td>
                        </tr>

                        <tr>
                            <th>Facebook Link</th>
                            <td> {{ $setting->s_facebook }} </td>
                        </tr>
                        <tr>
                            <th>Twitter Link</th>
                            <td> {{ $setting->s_twitter }} </td>
                        </tr>
                        <tr>
                            <th>Youtube link</th>
                            <td> {{ $setting->s_youtube_link }} </td>
                        </tr>
                        <tr>
                            <th>Linkedin Link</th>
                            <td> {{ $setting->s_linkedin }} </td>
                        </tr>
                       <tr>
                            <th>Googleplus Link</th>
                            <td> {{ $setting->s_google_plus }} </td>
                        </tr>
                         </tbody>
                    </table>
                </div>

            </div>

            </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection