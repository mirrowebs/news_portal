@extends('admin.layout')

@section('title', $title)

@section('headersscript')
    <script src="{{url('js/jquery.validate.min.js')}}"></script>
    <script src="{{url('admin/modules/settings/js/settings.js')}}"></script>
    <script src="{{url('admin/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            $('.ceditor').each(function (e) {
                CKEDITOR.replace(this.id, {
                    filebrowserBrowseUrl: '/admin/ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: '/admin/ckfinder/ckfinder.html?Type=Images',
                    filebrowserUploadUrl: '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                    filebrowserWindowWidth: '1000',
                    filebrowserWindowHeight: '700'
                });
            });
        });
    </script>
@endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Update settings
                <small>Manage settings</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{URL('/admin/settings')}}">Settings</a></li>
                <li class="active">Update settings</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-primary">
                        <br/>
                        <form method="POST" id="settings_validation" action="{{ url('/admin/settings/' . $setting->s_id) }}"
                              accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('admin.settings.form', ['submitButtonText' => 'Update'])

                        </form>

                    </div>

                </div>

            </div>
        </section>
    </div>


@endsection