@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 fwhite"><span class="fbold">iDREAM </span> <span class="flight">GALLERY</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('gallery')}}" class="fblue">Gallery</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <!-- image gallery -->
            <section class="gallery-img py-2">
                <div class="container">
                    <div class="row py-3 wow fadeInUp">
                        <div class="col-md-12">
                            <article class="subsectiontitle my-4">
                                <h4 class="h4 fbold m-0">Image <span class="fblue">Gallery</span></h4>
                                <p>Gallery all categories</p>
                            </article>
                        </div>
                    </div>
                    @if(sizeof($galleries)>0)
                    <div class="row py-3">
                        @foreach($galleries as $gallery)
                            @if(sizeof($gallery->galleryImages)>0)
                        <div class="col-md-4 gallist">
                            <figure>
                                <a href="{{route('gallery-details',['id' => $gallery->g_id,'alias' => $gallery->g_alias])}}"><img src="/uploads/gallery/thumbs/{{$gallery->g_image}}" class="img-fluid object-fit_cover "></a>
                            </figure>
                            <article>
                                <h6 class="h6 py-1"><a href="{{route('gallery-details',['id' => $gallery->g_id,'alias' => $gallery->g_alias])}}">{{$gallery->g_title}}</a></h6> <span class="fbold fblue">{{ sizeof($gallery->galleryImages) }} Images</span> </article>
                        </div>
                            @endif
                        @endforeach
                    </div>
                    @else
                        <div class="row py-3" align="center"> There is No galleries yet </div>
                    @endif
                </div>
            </section>
            <!--/ image gallery -->
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection