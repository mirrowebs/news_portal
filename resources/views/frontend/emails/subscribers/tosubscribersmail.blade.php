<!DOCTYPE html>
<html>

<head>
    <title>Thanks from iDream Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
            padding: 5px;
        }

    </style>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; padding:20px 10px;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <a href="http://idreammedia.com/" target="_blank"><img style="width:75px;" src="{{URL::asset('uploads/mails/logo.png')}}" alt="idream Media"></a>
            </td>
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <a href="https://www.facebook.com/" target="_blank"><img style="width:35px;" src="{{URL::asset('uploads/mails/fbicon.png')}}"></a>
                        </td>
                        <td>
                            <a href="https://www.facebook.com/" target="_blank"><img style="width:35px;" src="{{URL::asset('uploads/mails/linkedinicon.png')}}"></a>
                        </td>
                        <td>
                            <a href="https://www.facebook.com/" target="_blank"><img style="width:35px;" src="{{URL::asset('uploads/mails/utubeicon.png')}}"></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="background:#000;" colspan="3" height="50" align="center"> <a style="color:#fff; text-decoration: none;" href="http://idreammedia.com/">Visit Our Website</a> </td>
        </tr>
        <tr>
            <td colspan="3"> <img style="width:100%;" src="{{URL::asset('uploads/mails/thankuimg.jpeg')}}"> </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <h2>You've been added to our Mailing list.</h2>
                <p style="line-height: 30px;">We' re Happy to Report that you will be among the first to hear About New Arrivals, Big Events and Special Offers.</p>
                <h1 style="color:#0f9fd6;">Enjoy!</h1>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" style="background:#0f9fd6">
                <table>
                    <tr>
                        <td><a href="javascript:void(0)" style="font-size:13px; color:#fff; text-decoration: none; text-transform: uppercase;">About us</a></td>
                        <td><a href="javascript:void(0)" style="font-size:13px; color:#fff; text-decoration: none; text-transform: uppercase;">Movies</a></td>
                        <td><a href="javascript:void(0)" style="font-size:13px; color:#fff; text-decoration: none; text-transform: uppercase;">Contact Us</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <p style="font-size:12px; color:#666; text-align:justify; line-height: 18px;"><strong>Terms &amp; Conditions</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque ullam est dignissimos labore, enim similique quis architecto magnam tenetur at dolores, numquam dolore eius eligendi nemo porro nulla commodi quas rerum corrupti? Temporibus fugiat delectus sit reprehenderit vitae, laboriosam corporis, vel dignissimos distinctio, error itaque sapiente ipsam illum, exercitationem asperiores veritatis iusto minima ut ipsa perspiciatis quisquam nihil cumque. </p>
            </td>
        </tr>
    </table>
</div>
</body>

</html>
