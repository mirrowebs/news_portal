<!DOCTYPE html>
<html>

<head>
    <title>Thanks from iDream Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {}

        @media only screen and (max-width: 800px) {
            div {
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; background:#f3f3f3;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" style="padding:10px;">
                <a href="javascript:void(0)"><img src="{{URL::asset('uploads/mails/logo.png')}}"></a>
            </td>
        </tr>
        <tr>
            <td align="center"> <img src="{{URL::asset('uploads/mails/ban2.png')}}" style="width:100%;"> </td>
        </tr>
        <tr>
            <td align="center" style="background:#f9f9f9;">
                <table width="75%">
                    <tr>
                        <td align="center">
                            <h2>Contact Form Submission Information</h2>
                            <p style="font-size:14px; line-height:22px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, amet! Nostrum laudantium excepturi facilis tempore ut modi quia, amet possimus ipsum, dicta nam rem mollitia!</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%" align="center">
                                <tr>
                                    <td style="color:#888;">Name</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$name}}</td>
                                </tr>
                                <tr>
                                    <td style="color:#888;">Contact Number</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$phone}}</td>
                                </tr>
                                <tr>
                                    <td style="color:#888;">EMail Address</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$email}}</td>
                                </tr>
                                <tr>
                                    <td style="color:#888;">Message</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$text}}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <p style="font-size:12px; color:#666;"><strong>Terms and Conditions</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, facilis, quo. Non sit beatae asperiores, a culpa ad adipisci sunt deserunt, quas consectetur dolorum vitae, placeat laudantium doloribus. Obcaecati ratione quibusdam, eos soluta sunt minus facilis repellendus illo, quia, odit voluptate accusamus esse quisquam, praesentium ducimus deleniti sequi sed recusandae. Delectus ad itaque, vel sunt natus odit atque, eos, accusantium consequuntur fuga unde velit! </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>

</html>
