<!DOCTYPE html>
<html>

<head>
    <title>Thanks from iDream Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
            padding: 5px;
        }

        @media only screen and (max-width: 800px) {
            div {
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; padding:25px; background:#eaeff2;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <a href="http://idreammedia.com/"><img src="{{URL::asset('uploads/mails/logo.png')}}"></a>
            </td>
        </tr>
        <tr>
            <td align="center">
                <h1 style="font-weight: normal; margin:0;">Hi <strong style="color:#0098eb">{{$name}}</strong></h1>
                <h3 style="padding:10px 50px; line-height: 30px; margin: 0;">Thank you for Your Submission, We will contact you soon</h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="90%" cellpadding="0" cellspacing="0" style="background:#fff; padding:20px; margin:0 0 10px 0;">
                    <tr>
                        <td align="center"><img style="width:96px;" src="{{URL::asset('uploads/mails/mailicon.png')}}"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <h4 style="font-size:16px; line-height: 25px; font-weight: normal;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae sit porro voluptatibus voluptatem rerum vitae iusto qui, fugiat ea eum Lorem ipsum dolor sit amet.</h4>
                            <hr>
                            <p style="font-size:14px;">You can access WorkAngel online or on any device by going to</p> <a style="background:#0291e0; display: block; width:125px; padding:10px 25px; text-align: center; color:#fff; text-decoration: none; text-transform: uppercase;" href="http://idreammedia.com/">visit Website</a> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="90%" cellpadding="0" cellspacing="0" style="background:#fff; padding:20px;">
                    <tr>
                        <td align="center">
                            <h2 style="line-height: 25px; font-weight: normal;">Get the iDream Media App! </h2>
                            <p style="font-size:14px;">Get the most of iDream media by installing the mobile app.</p>
                            <p style="font-size:14px;">You can log in by suing your existing emails address an password. </p>
                            <p>
                                <a href="https://play.google.com/store/apps/details?id=idreammedia.iw"><img src="{{URL::asset('uploads/mails/googleplay.png')}}" style="width:120px;" alt="google play store"></a>
                                <a href="https://itunes.apple.com/app/id557521403"><img src="{{URL::asset('uploads/mails/googleplay.png')}}" style="width:120px;" alt="google play store"></a>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td>
                            <a href="https://www.facebook.com/iDreamMedia/"><img src="{{URL::asset('uploads/mails/fbicon.png')}}" style="width:30px;"></a>
                        </td>
                        <td>
                            <a href="https://twitter.com/iDreamMedia"><img src="{{URL::asset('uploads/mails/twittericon.png')}}" style="width:30px;"></a>
                        </td>
                        <td>
                            <a href="https://in.linkedin.com/company/idream-media-inc."><img src="{{URL::asset('uploads/mails/linkedinicon.png')}}" style="width:30px;"></a>
                        </td>
                        <td>
                            <a href="https://www.youtube.com/results?search_query=idream+media"><img src="{{URL::asset('uploads/mails/utubeicon.png')}}" style="width:30px;"></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td> <a style="text-decoration: none; font-size:14px; color:#000;" href="javascript:void(0)">About us</a> </td>
                        <td> <a style="text-decoration: none; font-size:14px; color:#000;" href="javascript:void(0)">Serivces</a> </td>
                        <td> <a style="text-decoration: none; font-size:14px; color:#000;" href="javascript:void(0)">Contact</a> </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>

</html>
