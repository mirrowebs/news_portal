@extends('frontend.layouts.app')
@section('title', $title)
@section('headerscripts')
    <script src="{{url('admin/modules/contact-submissions/js/contact-submissions.js')}}"></script>
@endsection

@section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 fwhite"><span class="fbold">CONTACT </span> <span class="flight">US</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('contactus')}}" class="fblue">Contact</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <!-- image gallery -->
            <section class="gallery-img py-2">
                <div class="container">
                    <div class="row py-3 wow fadeInUp">
                        <article class="subsectiontitle my-4">
                            <h4 class="h4 fbold m-0">Get <span class="fblue">in Touch</span></h4>
                            <p>Reach us</p>
                        </article>
                    </div>
                    <div class="row wow fadeInUp">
                        <!-- left column-->
                        <div class="col-md-12">
                            @if (Session::has('flash_message'))
                                <br/>
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ Session::get('flash_message' ) }}</strong>
                                </div>
                            @endif
                            <form class="form-contact row" method="POST" id="contact_validation" action="{{ route('contactus') }}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('cs_name') ? 'has-error' : ''}}">
                                        <label>Your Name</label>
                                        <input type="text" class="form-control" name="cs_name" id="cs_name"
                                               placeholder="Your name">
                                        {!! $errors->first('cs_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group {{ $errors->has('cs_contact_number') ? 'has-error' : ''}}">
                                        <label>Contact Number</label>
                                        <input type="text" class="form-control" placeholder="Contact Number"
                                               name="cs_contact_number" id="cs_contact_number">
                                        {!! $errors->first('cs_contact_number', '<p class="help-block">:message</p>') !!} </div>
                                    <div class="form-group {{ $errors->has('cs_email') ? 'has-error' : ''}}">
                                        <label>EMail Address</label>
                                        <input type="text" class="form-control" placeholder="Valid Email"
                                               name="cs_email" id="cs_email">
                                        {!! $errors->first('cs_email', '<p class="help-block">:message</p>') !!} </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('cs_message') ? 'has-error' : ''}}">
                                        <label>Message</label>
                                        <textarea class="form-control" rows="7" name="cs_message"
                                                  id="cs_message"></textarea>
                                        {!! $errors->first('cs_message', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" type="submit">SUBMIT</button>
                                </div>
                            </form>
                        </div>
                        <!--/ left column -->
                        <!-- right column-->
                        <div class="col-md-4 pt-5">
                            <h5 class="h5 fbold">India</h5>
                            <table class="table rightcontact mt-3">
                                <tr>
                                    <td style="border-top:0;"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
                                    <td style="border-top:0;">{{ $settings->s_ind_address }}</td>
                                </tr>
                                @if($settings->s_ind_email!='')
                                <tr>
                                    <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                                    <td>{{ $settings->s_ind_email }}</td>
                                </tr>
                                @endif
                                @if($settings->s_ind_mobile!='')
                                <tr>
                                    <td><i class="fa fa-phone-square" aria-hidden="true"></i></td>
                                    <td>{{ $settings->s_ind_mobile }}</td>
                                </tr>
                                @endif
                                <!--
                                <tr>
                                    <td><i class="fa fa-fax" aria-hidden="true"></i></td>
                                    <td>{{ $settings->s_ind_phone }}</td>
                                </tr>
-->
                            </table>
                        </div>
                        <div class="col-md-4 pt-5">
                            <h5 class="h5 fbold">USA</h5>
                            <table class="table rightcontact mt-3">
                                <tr>
                                    <td style="border-top:0;"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
                                    <td style="border-top:0;">{{ $settings->s_usa_address }}</td>
                                </tr>
                                @if($settings->s_usa_email!='')
                                <tr>
                                    <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                                    <td>{{ $settings->s_usa_email }}</td>
                                </tr>
                                @endif
                                @if($settings->s_usa_mobile!='')
                                <tr>
                                    <td><i class="fa fa-phone-square" aria-hidden="true"></i></td>
                                    <td>{{ $settings->s_usa_mobile }}</td>
                                </tr>
                                @endif
                                <!--
                                <tr>
                                    <td><i class="fa fa-fax" aria-hidden="true"></i></td>
                                    <td>{{ $settings->s_usa_phone }}</td>
                                </tr>
-->
                            </table>
                        </div>
                        <!--/ right columb-->
                    </div>
                </div>
            </section>
            <!--/ image gallery -->
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection