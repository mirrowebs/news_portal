@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">Film &amp; Video </span> <span class="flight">Editing</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Services</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('editing')}}" class="fblue">Film &amp; Video Editing </a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-md-12 text-center"> <img src="{{URL::asset('frontend/img/videoediting-img.png')}}" class="img-fluid"> </div>
                </div>
                <div class="row wow fadeInUp">
                    <div class="col-md-12">
                        <h5 class="h5 fbold py-2">Professional Film &amp; Video Editing Services</h5>
                        <p class="text-justify py-2">Editing can make or break your content, whether it's a home video or a Feature Film. We undertake editing services on an approval basis in our editing suites. Movies are made in the post, and we help take your content to the next level, whether it's a movie, short film, wedding video, advertisement, corporate video, testimonial, tribute video or a training video. We also offer VFX and other design post based on your requirements. </p>
                    </div>
                </div>
                <div class="row py-5 wow fadeInUp">
                    <div class="col-md-6">
                        <article class="subsectiontitle py-2">
                            <h4 class="h4 fbold m-0">High Definition <span class="fblue">Video Editing Services</span></h4>
                            <p>iDream Media has the capabilities and infrastructure required to edit and transform any raw footage into crisp and contemporary videos within a short time.</p>
                        </article>
                        <!-- table-->
                        <table class="table-video mt-3" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%">Corporate Videos</td>
                                <td width="50%">Client Testimonial Videos</td>
                            </tr>
                            <tr>
                                <td width="50%">Training Videos</td>
                                <td width="50%">Event Videos</td>
                            </tr>
                            <tr>
                                <td width="50%">TV Commercials</td>
                                <td width="50%">Short Films</td>
                            </tr>
                            <tr>
                                <td width="50%">Educational Videos</td>
                                <td width="50%">Documentaries</td>
                            </tr>
                            <tr>
                                <td width="50%">TV Shows</td>
                                <td width="50%">Wedding Videos</td>
                            </tr>
                            <tr>
                                <td width="50%">Real Estate Drone Video</td>
                                <td width="50%">Explainer Videos</td>
                            </tr>
                            <tr>
                                <td width="50%">YouTube Videos</td>
                                <td width="50%">Real Estate Video Editing</td>
                            </tr>
                        </table>
                        <!--/ table -->
                    </div>
                    <div class="col-md-6">
                        <!-- contact form for social media management-->
                        <div class="form-social p-3">
                            <div class="row pt-1">
                                <div class="col-md-12">
                                    <h5 class="h5 fbold py-2">Contact for Film &amp; Editing</h5>
                                </div>
                            </div>
                            @if (Session::has('flash_message'))
                                <br/>
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ Session::get('flash_message' ) }}</strong>
                                </div>
                            @endif
                            <form method="POST" id="socialmedia_validation" action="{{route('editing')}}" accept-charset="UTF-8" enctype="multipart/form-data" >
                                {{ csrf_field() }}
                                <div class="row py-1">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('cs_website') ? 'has-error' : ''}}">
                                            <label>Website</label>
                                            <input type="text" class="form-control" name="cs_website" id="cs_website" >
                                            {!! $errors->first('cs_name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('cs_name') ? 'has-error' : ''}}">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="cs_name" id="cs_name" >
                                            {!! $errors->first('cs_name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('cs_email') ? 'has-error' : ''}}">
                                            <label>E-mail</label>
                                            <input type="text" class="form-control" name="cs_email" id="cs_email" >
                                            {!! $errors->first('cs_email', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('cs_contact_number') ? 'has-error' : ''}}">
                                            <label>Phone</label>
                                            <input type="text" class="form-control" name="cs_contact_number" id="cs_contact_number" >
                                            {!! $errors->first('cs_contact_number', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('cs_company_name') ? 'has-error' : ''}}">
                                            <label>Company name</label>
                                            <input type="text" class="form-control" name="cs_company_name" id="cs_company_name" >
                                            {!! $errors->first('cs_company_name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('cs_location') ? 'has-error' : ''}}">
                                            <label>Location</label>
                                            <input type="text" class="form-control" name="cs_location" id="cs_location" >
                                            {!! $errors->first('cs_location', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class="col-md-12 {{ $errors->has('cs_message') ? 'has-error' : ''}}">
                                        <label>Put Your Ideas</label>
                                        <textarea rows="4" cols="50" name="cs_message" id="cs_message" > </textarea>
                                        {!! $errors->first('cs_message', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class="col-md-12">
                                        <button class="btn btn-block btn-success" type="submit">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--/ contact formfor social media management-->
                    </div>
                </div>
            </div>
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection