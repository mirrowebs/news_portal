@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
<?php
//        $id='4ThhZTOg8a0';
//$api_key = 'AIzaSyC1cwMHcy0_o_s1I5W0wYM-xitwN2_cD2I';
//$api_url = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,statistics&id=' . $id . '&key=' . $api_key;
//$data = json_decode(file_get_contents($api_url,true));


//echo $data->title;


//$items = $json -> data -> items;


//echo $data->items[0].snippet.title;
//
//
//echo "<pre>";
//print_r($data);
//echo "<pre>";

//        echo "<pre>";
//print_r($data->items[0]->snippet->localized);
//echo "</pre>";
//
//echo $data->items[0]->snippet->localized->title;


?>

    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h2 class="h2 fwhite"><span class="fbold">iDREAM </span> <span class="flight">INTERVIEWS</span></h2> </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('interviews')}}" class="fblue">Interviews</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
    <article class="subsectiontitle mt-2">
        <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
        <p>Aenean suscipit eget mi act</p>
    </article>
    title -->
        <section class="subpage-body py-4">
            <!-- what we provide in social media management -->
            <section class="soundseervices py-4">
                <div class="container">
                    <div class="row pb-5 wow fadeInUp">
                        <div class="col-md-12">
                            <article class="subsectiontitle mt-2">
                                <h4 class="h4 fbold m-0">iDream <span class="fblue">Interviews</span></h4>
                                <p>Celebrities, Sports, Movies</p>
                            </article>
                        </div>
                        <div class="col-md-12">
                            <p class="text-justify py-2">Talking Movies, Talking Politics, Crime Diaries and everything in between. The Movers and Shakers of the town come undone and reveal what they've never before, and our interviewers end up with the most exclusive snippets from the most reclusive of them all.</p>
                            <p class="text-justify py-2">Our long form interviews started a trend, pushing the boundaries of journalism and adding intrinsic value to pop culture conversations. iDream Interviews changed the way interviews are done. 1200 and counting, a new face added every day. These are the chroniclers of time.</p>
                        </div>
                    </div>

                    {{--@foreach($interviews as $interviews2)--}}
                        {{--<h1> {{ $interviews2->cat_status }} </h1>--}}
                    {{--@endforeach--}}

                    <div class="row wow fadeInUp">
                        <div class="col-md-12">
                            <ul class="simplefilter" id="simplefilter">
                                <li class="active" data-filter="all">All</li>
                                @if(sizeof($interviews)>0)
                                    @foreach($interviews as $interviews)
                                        <li data-filter="{{ $interviews->cat_id }}">{{ $interviews->cat_name }}</li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>

                    <?php
                    $api_key = 'AIzaSyC1cwMHcy0_o_s1I5W0wYM-xitwN2_cD2I';
                    ?>
                    <div class="filtr-container" id="filtr-container">
                        <div class="row wow fadeInUp">
                            @foreach($interviews2 as $interviews2)
                                    <?php

                                    $videos=$interviews2->cat_videos;
                                    $var=explode(',',$videos);
                                    foreach($var as $videotag)
                                    {
                                            if (in_array($videotag, $var)) {
                                                $category=$interviews2->cat_id;
                                            }
                                $api_url = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,statistics&id=' . $videotag . '&key=' . $api_key;
                                $data = json_decode(file_get_contents($api_url,true));
                                        ?>
                                <div class="col-md-3 filtr-item" data-category="<?php echo $category; ?>" data-sort="Busy streets"><a href="https://www.youtube.com/watch?v=<?php echo $videotag; ?>" target="_blank"> <img class="img-fluid" src="https://img.youtube.com/vi/<?php echo $videotag; ?>/hqdefault.jpg"> </a><a href="https://www.youtube.com/watch?v=<?php echo $videotag; ?>" target="_blank"><?php echo $data->items[0]->snippet->localized->title; ?></a> </div>
                                        <?php
                                    }
                                    ?>
                            @endforeach
                                @if(sizeof($interviews3)>0)
                                    @foreach($interviews3 as $interviews3)
                                <div class="col-md-3 filtr-item" data-category="{{ $interviews3->cat_id }}">
                                    <a href="{{ $interviews3->cat_link }}" class="morebtn text-center py-4">
                                        <h6 class="h6 fwhite text-center">{{ $interviews3->cat_name }} <br/> View All Videos </h6> <span class="h2 fbold fwhite">{{ $interviews3->cat_video_count }}</span> </a>
                                </div>
                                    @endforeach
                                @endif
                        </div>
                    </div>







                    <!-- idream interviewer -->
                    <div class="row pb-4 wow fadeInUp">
                        <div class="col-md-12">
                            <article class="subsectiontitle mt-4">
                                <h4 class="h4 fbold m-0">iDream <span class="fblue">Interviewer</span></h4>
                                <p>Our Interviewer Team </p>
                            </article>
                        </div>
                    </div>
                    <!--/ idream interviewer-->
                </div>
                <!-- idream interviewer slider -->





                <div class="interviewer-slider">
                    <!-- scroll -->
                    <div class="container strengthscolumns mt-5">
                        <div id="slide03">
                            <div class="my-4">
                                <!-- column-->
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=FQmyL6s73EM&list=PLSPuDpldWomuputQCshJ4ykfRlvpU86CP" target="_blank"><img src="{{URL::asset('frontend/img/tnrimg.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">TNR</h6> </article>
                                </div>
                                <!--/ column -->
                                <!-- column-->
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=pIp-EfcR0Sg&list=PLkY9zc0b0zviQu_AKKYsCSCt1gbP6KrR-" target="_blank"><img src="{{URL::asset('frontend/img/nagaraju.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Nagaraju B.Com</h6> </article>
                                </div>
                                <!--/ column -->
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=v5RcRr2F1_c&list=PLSPuDpldWomuVKo-5swenBPIxGfmT1IS3" target="_blank"><img src="{{URL::asset('frontend/img/anjali.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Anjali</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=97edwh6WV0k&list=PLSPuDpldWomtYk4waLhsH-8d5xliJZIbD" target="_blank"><img src="{{URL::asset('frontend/img/charani.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Charani</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=XESQ8Oy1g80&list=PLSPuDpldWomuBU92EqJTD4PMgQwGBfjP7" target="_blank"><img src="{{URL::asset('frontend/img/harshini.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Harshini </h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=ejAuNwrznAU&list=PLSPuDpldWoms-uUIgRLCuYF9sD9EcfuYb" target="_blank"><img src="{{URL::asset('frontend/img/bhargav.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6"> Bhargav</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=URmw4UrYtwc&list=PLSPuDpldWomvAdt_6OWBSHcwjTWHE8sya" target="_blank"><img src="{{URL::asset('frontend/img/ariyana.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Ariyana</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=VwJt93TS7UE&list=PLSPuDpldWomuOhC3NPw2TtccLAfJQehOp" target="_blank"><img src="{{URL::asset('frontend/img/sandy.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Sandy</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=jj-k0229gGA&list=PLSPuDpldWomsf7vdoVij-fUEOOvbvGz48" target="_blank"><img src="{{URL::asset('frontend/img/komali.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Komali</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=yCf6L06CSX0&list=PLSPuDpldWomvV3mWLbalpftxIzGBYU29y" target="_blank"><img src="{{URL::asset('frontend/img/swethareddy.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Swetha Reddy</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=x9skn25fdCc&list=PLSPuDpldWomsv1dkfbpeyLgrrRLPFRG_7" target="_blank"><img src="{{URL::asset('frontend/img/prema.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Prema</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=OqoUDBucP8o&list=PLSPuDpldWomvQIwYBe1THloTEYqKdMfce" target="_blank"><img src="{{URL::asset('frontend/img/yamunakishore.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Yamuna Kishore</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=3ng7NUj7jxo&list=PLSPuDpldWomuIQ9XonNxF5muHNaSi_kVu" target="_blank"><img src="{{URL::asset('frontend/img/kaumudi.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Kaumudi</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=zxSLVRu0ayg&list=PLSPuDpldWoms8K_taxBX_6GBlojWwmY3-" target="_blank"><img src="{{URL::asset('frontend/img/swapna.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Swapna</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=4Dy5EWnnfx4&list=PLSPuDpldWomvKnaOHLEvAMHVwJWf0Xanx" target="_blank"><img src="{{URL::asset('frontend/img/hemanth.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Hemanth</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=VtJVmMaZ5yc&list=PLSPuDpldWomvSXR90pvCKqvJ-A3eIin9o" target="_blank"><img src="{{URL::asset('frontend/img/vrinda.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Vrinda</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=uHz05pj1h9g&list=PLkY9zc0b0zvhKmnN-upgBbM5Q5o-qCA8j" target="_blank"><img src="{{URL::asset('frontend/img/vijayacheeli.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Vijaya Cheeli</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=AQ2gzNOA9yQ&list=PLkY9zc0b0zvjhzc3DdPWt4VjzgnM6B836" target="_blank"><img src="{{URL::asset('frontend/img/nagesh.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Nagesh</h6> </article>
                                </div>
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=emNuSULbUH0&list=PLkY9zc0b0zvgejEextKCQ7Q5kXq8Mst_U" target="_blank"><img src="{{URL::asset('frontend/img/pradeep.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Pradeep </h6> </article>
                                </div>
                                <!-- column-->
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=YOUxd7jJfNE&list=PLkY9zc0b0zvhFzYWgRTqJ9CDqE6N4qejn" target="_blank"><img src="{{URL::asset('frontend/img/vikrampoola.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Vikram Poola</h6> </article>
                                </div>
                                <!--/ column -->
                                <div class="coreteam-col">
                                    <figure>
                                        <a href="https://www.youtube.com/watch?v=BbA4poMzJOM&list=PLkY9zc0b0zvgK-3cOE0g4yeBcZ3nRg9yG" target="_blank"><img src="{{URL::asset('frontend/img/muralidhar.jpg')}}" class="img-fluid"></a>
                                    </figure>
                                    <article class="p-3">
                                        <h6 class="h6">Muralidhar</h6> </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ scroll -->
                </div>
                <!--/ idream interviewer slider -->
            </section>
        </section>
        <!--/ sub page body -->
    </section>














    <!--/ main -->@endsection