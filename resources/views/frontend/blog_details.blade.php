@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->





    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">iDream </span> <span class="flight">Blog</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="{{route('blog')}}" class="fwhite">Blog</a></li>
                            <li class="breadcrumb-item active float-left"><a href="javascript:void(0)" class="fblue">{{ $blog->b_title }}</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-md-9">
                        <div class="card blogdetailimg mb-4"> <img class="card-img-top img-fluid object-fit_cover" src="/uploads/blog/{{$blog->b_image}}" alt="{{$blog->b_title}}">
                            <div class="card-body">
                                <h5 class="card-title h5 fbold">{{$blog->b_title}}</h5>
                                <p class="text-justify pb-2">{{$blog->b_description}}</p>
                            </div>
                            <div class="card-footer text-muted"> Posted on {{ ($blog->created_at) ? \Carbon\Carbon::parse($blog->created_at)->format('d-m-Y H:i:s') : '-'  }} </div>
                        </div>
                    </div>
                    <!-- right categories-->
                    <div class="col-md-3">
                        <!-- Search Widget -->
                        {{--<div class="card mb-4">--}}
                            {{--<h5 class="card-header">Search</h5>--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="input-group">--}}
                                    {{--<input type="text" class="form-control" placeholder="Search for..."> <span class="input-group-btn">--}}
                  {{--<button class="btn btn-secondary" type="button">Go!</button>--}}
                {{--</span> </div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="card">
                            <h5 class="card-header">Recent Posts</h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-unstyled mb-0 blogdetailrtlist">
                                            @if(sizeof($blogs)>0)
                                                @foreach($blogs as $blogs)
                                            <li> <a href="{{route('blogview',['id' => $blogs->b_id])}}">{{$blog->b_title}}</a> </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ right categories -->
                </div>
            </div>
        </section>
        <!--/ sub page body -->
    </section>




    <!--/ main -->@endsection