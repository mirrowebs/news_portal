@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 fwhite"><span class="fbold">iDREAM </span> <span class="flight">APPS</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Our Products</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('our-apps')}}" class="fblue">iDream Apps</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <!-- row -->
                <div class="row pb-4 rowbrd mb-5">
                    <!-- appcolumn-->

                    @if(sizeof($apps)>0)
                        @foreach($apps as $apps)


                    <div class="col-md-6 wow fadeInUp appcolumn">
                        <div class="appcol p-3">
                            <div class="row">
                                <div class="col-md-4">
                                    <figure class="appcolimg">
                                        <a><img src="/uploads/apps/{{$apps->app_image}}" class="img-fluid" alt="{{$apps->app_title}}"></a>
                                    </figure>
                                </div>
                                <div class="col-md-8">
                                    <h3 class="h3 flight mb-2">{{$apps->app_title}}</h3>
                                    <ul class="list-inline weblist applist">

                                        <li class="list-inline-item fbold">{{$apps->app_sub_title}}</li>

                                    </ul>
                                    <ul class="appos py-4">
                                        @if($apps->app_android_link!='')
                                        <li class="p-1">
                                            <a href="{{$apps->app_android_link}}" target="_blank"><img src="{{URL::asset('frontend/img/googleplay.png')}}" class="img-fluid" alt="{{$apps->app_title}}"></a>
                                        </li>
                                        @endif
                                        @if($apps->app_ios_link!='')
                                        <li class="p-1">
                                            <a href="{{$apps->app_ios_link}}" target="_blank"><img src="{{URL::asset('frontend/img/appstorelogo.png')}}" class="img-fluid" alt="{{$apps->app_title}}"></a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <p class="py-1 text-justify">{{$apps->app_description}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach
                @endif
                    <!-- /app column -->
                    <!-- appcolumn-->

                    <!-- /app column -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection