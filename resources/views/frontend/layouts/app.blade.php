      <!doctype HTML>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Free Web tutorials">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="John Doe">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('frontend.includes.styles')
    @yield('headerstyles')
</head>

<body id="" class="">

<!-- header -->
@include('frontend.includes.header')
<!--/ header -->

@yield('content')
<!--main section -->


<!-- footer -->
@include('frontend.includes.footer')

@yield('headerscripts')
<!--/ footer -->
</body>

</html>
