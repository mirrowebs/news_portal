@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->





    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">iDream </span> <span class="flight">Events</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="{{route('events')}}" class="fwhite">Events</a></li>
                            <li class="breadcrumb-item active float-left"><a href="" class="fblue">{{$event->e_title}}</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <!-- figure -->
                <div class="row wow fadeInUp">
                    <div class="col-md-12 img-fluid text-center">

                        <figure class=""><img class="img-fluid object-fit_cover" src=/uploads/events/{{$event->e_image}}></figure>
                        <article class="py-3">
                            <h5 class="h5 fbold">{{$event->e_title}}</h5> <small class="fblue">{{$event->e_schedule_info}}, at {{$event->e_place}}</small> </article>
                            <p class="text-justify py-2"><?php echo $event->e_description; ?> </p>
                    </div>
                </div>
                <!--/ figure-->
            </div>
            <!-- gallery -->
            @if(sizeof($gallery)>0)
            <div class="eventgallery wow fadeInUp">
                <div class="container">
                    <h5 class="h5 fbold py-4">Picture Gallery</h5>
                </div>
                <div class="wow fadeInUp" data-nanogallery2>
                    @foreach($gallery as $gal)
                    <a href="/uploads/events/{{$gal->ei_image}}" data-ngThumb="/uploads/events/thumbs/{{$gal->ei_image}}"></a>
                    @endforeach
                </div>
            </div>
            @endif
            <!--/ gallery -->
        </section>
        <!--/ sub page body -->
    </section>




    <!--/ main -->@endsection