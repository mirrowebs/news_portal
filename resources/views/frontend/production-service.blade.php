@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">Production  </span> <span class="flight"> Services</span></h1>
                        </article>
                    </div>
                    <div class="col-md-4 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Services</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('production-service')}}" class="fblue"> Production </a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <!-- what we provide in social media management -->
            <section class="soundseervices py-4">
                <div class="container">
                    <div class="row py-3 wow fadeInUp">
                        <div class="col-md-12 text-xs-center">
                            <article class="subsectiontitle mt-2">
                                <h4 class="h4 fbold m-0">Production <span class="fblue">Services at iDream </span></h4>
                                <p>Our Teams to Help You Create the Content You Want</p>
                            </article>
                        </div>
                    </div>
                    <div class="row py-4 wow fadeInUp">
                        <div class="col-md-4 text-xs-center">
                            <figure><img src="{{URL::asset('frontend/img/productionicon02.png')}}"></figure>
                            <h6 class="h6 py-2 fbold">Production Services</h6>
                            <p class="text-justify text-xs-center pb-xs-15">As a Creator with a discernible talent, you may need help with putting yourself out there. From camera teams to equipment, studios, green mat, sets and locations, we help you put together the best possible content to showcase your uniqueness. </p>
                        </div>
                        <div class="col-md-4 text-xs-center">
                            <figure><img src="{{URL::asset('frontend/img/productionicon03.png')}}"></figure>
                            <h6 class="h6 py-2 fbold">Post Production Services</h6>
                            <p class="text-justify text-xs-center pb-xs-15">After you shoot, then what? Our teams will edit that content in our editing suites, and our dubbing studio and mixing can take your sound design to the next level. Our design teams can spruce up your post production to the next level because as they say, movies are made in post.</p>
                        </div>
                        <div class="col-md-4 text-xs-center">
                            <figure><img src="{{URL::asset('frontend/img/productionicon01.png')}}"></figure>
                            <h6 class="h6 py-2 fbold">Social Media and Content Management</h6>
                            <p class="text-justify text-xs-center pb-xs-15">Push that content out to reach the widest possible demographic with the most effective distribution of content that is personalised and curated through diverse digital platforms. Our teams will help you market your content in various platforms.</p>
                        </div>
                    </div>
                </div>
            </section>


            <section class="yneedsocial py-3">
                <div class="container">
                    <div class="row py-4 wow fadeInUp">
                        <div class="col-md-12">
                            <!-- contact form for social media management-->
                            <div class="form-social p-3">
                                <div class="row pt-1">
                                    <div class="col-md-12">
                                        <h5 class="h5 fbold py-2">Contact for Production Service</h5>
                                    </div>
                                </div>
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                                <form method="POST" id="socialmedia_validation" action="{{route('production-service')}}" accept-charset="UTF-8" enctype="multipart/form-data" >
                                    {{ csrf_field() }}
                                    <div class="row py-1">
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_website') ? 'has-error' : ''}}">
                                                <label>Website</label>
                                                <input type="text" class="form-control" name="cs_website" id="cs_website" >
                                                {!! $errors->first('cs_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_name') ? 'has-error' : ''}}">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="cs_name" id="cs_name" >
                                                {!! $errors->first('cs_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row py-1">
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_email') ? 'has-error' : ''}}">
                                                <label>E-mail</label>
                                                <input type="text" class="form-control" name="cs_email" id="cs_email" >
                                                {!! $errors->first('cs_email', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_contact_number') ? 'has-error' : ''}}">
                                                <label>Phone</label>
                                                <input type="text" class="form-control" name="cs_contact_number" id="cs_contact_number" >
                                                {!! $errors->first('cs_contact_number', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row py-1">
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_company_name') ? 'has-error' : ''}}">
                                                <label>Company name</label>
                                                <input type="text" class="form-control" name="cs_company_name" id="cs_company_name" >
                                                {!! $errors->first('cs_company_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_location') ? 'has-error' : ''}}">
                                                <label>Location</label>
                                                <input type="text" class="form-control" name="cs_location" id="cs_location" >
                                                {!! $errors->first('cs_location', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row py-1">
                                        <div class="col-md-12 {{ $errors->has('cs_message') ? 'has-error' : ''}}">
                                            <label>Put Your Ideas</label>
                                            <textarea rows="4" cols="50" name="cs_message" id="cs_message" > </textarea>
                                            {!! $errors->first('cs_message', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="row py-1">
                                        <div class="col-md-12">
                                            <button class="btn btn-block btn-success" type="submit">SUBMIT</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--/ contact formfor social media management-->
                        </div>
                    </div>
                </div>
            </section>


            <!-- image gallery -->
            <section class="gallery-img py-2">
                <div class="container">
                    <div class="row py-3 wow fadeInUp">
                        <div class="col-md-12 text-xs-center">
                            <article class="subsectiontitle my-4">
                                <h4 class="h4 fbold m-0">Production <span class="fblue">Gallery</span></h4>
                                <p>Film Production Features We Providing</p>
                            </article>
                        </div>
                    </div>
                </div>               
                    <div class="wow fadeInUp" data-nanogallery2>
                        <a href="{{URL::asset('frontend/img/gallery/progal01.jpg')}}" data-ngThumb="{{URL::asset('frontend/img/gallery/progal01.jpg')}}"></a>
                        <a href="{{URL::asset('frontend/img/gallery/progal02.jpg')}}" data-ngThumb="{{URL::asset('frontend/img/gallery/progal02.jpg')}}"></a>
                        <a href="{{URL::asset('frontend/img/gallery/progal03.jpg')}}" data-ngThumb="{{URL::asset('frontend/img/gallery/progal03.jpg')}}"></a>
                        <a href="{{URL::asset('frontend/img/gallery/progal04.jpg')}}" data-ngThumb="{{URL::asset('frontend/img/gallery/progal04.jpg')}}"></a>
                        <a href="{{URL::asset('frontend/img/gallery/progal05.jpg')}}" data-ngThumb="{{URL::asset('frontend/img/gallery/progal05.jpg')}}"></a>
                        <a href="{{URL::asset('frontend/img/gallery/progal06.jpg')}}" data-ngThumb="{{URL::asset('frontend/img/gallery/progal06.jpg')}}"></a>
                        <a href="{{URL::asset('frontend/img/gallery/progal07.jpg')}}" data-ngThumb="{{URL::asset('frontend/img/gallery/progal07.jpg')}}"></a>
                    </div>               
            </section>
            <!--/ image gallery -->
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection