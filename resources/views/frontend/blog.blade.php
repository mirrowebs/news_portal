@extends('frontend.layouts.app') @section('title', $title) @section('content')

    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">iDream </span> <span class="flight">Blog</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="index.php" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item active float-left"><a href="javascript:void(0)" class="fblue">Blog</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- card row -->
                        <div class="row mt-5">
                            <!-- blog column-->

                            @if(sizeof($blog)>0)
                                @foreach($blog as $blog)
                            <div class="wow fadeInUp col-md-4 blogcol mt-3">

                                <a href="{{route('blogview',['id' => $blog->b_id])}}" target="_blank"><img class="card-img-top object-fit_cover" src="/uploads/blog/{{$blog->b_image}}" alt="Title Blog"></a>

                                <div class="card-body">
                                    <h6 class="card-title h6 fbold"><a href="{{route('blogview',['id' => $blog->b_id])}}" class="fblue" target="_blank">{{$blog->b_title}}</a></h6>
                                    <p class="card-text"><?php echo substr($blog->b_description, 0, 200)." ............"; ?></p>
                                </div>
                                <div class="card-footer"> <small class="fwhite">Posted on {{ ($blog->created_at) ? \Carbon\Carbon::parse($blog->created_at)->format('d-m-Y H:i:s') : '-'  }}</small> </div>
                            </div>
                            @endforeach

                        @else
<div class="text-center">
                                    <h4 class="fblue text-center"> There is no Blogs yet. </h4>
</div>

                        @endif
                            <!--/ blog column -->

                            <!--/ blog column-->
                        </div>
                        <!--/ card row -->
                        <!-- card row -->

                        <!--/ card row -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ sub page body -->
    </section>



    <!--/ main -->@endsection