@extends('frontend.layouts.app') @section('title', $title) @section('content')

    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h2 class="h2 text-uppercase fwhite"><span class="fbold">Web </span> <span class="flight">Series</span></h2>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Original Content</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('webseries')}}" class="fblue">Web Series </a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <section class="subpage-body py-4">
            <!-- what we provide in social media management -->
            <section class="soundseervices py-4">
                <div class="container">
                    <div class="row py-3 wow fadeInUp">
                        <div class="col-md-12 text-xs-center">
                            <article class="subsectiontitle mt-2">
                                <h4 class="h4 fbold m-0">iDream <span class="fblue">Web Series</span></h4>
                                <p>Rib-tickling, Belt-buckling, Chuck-chuckling Sagas</p>
                            </article>
                        </div>
                    </div>
                    <div class="row py-2 wow fadeInUp">
                        <div class="col-md-12">
                            <p class="text-justify">From unscripted content done in front of a webcam to earning Primetime Emmy nominations, Web video content has come a long, long way. Our iDream series kickstarted in 2016 and we built our way from multiple series, channels, numerous episodes on YouTube to making a foray into Television and now OTT platforms. The creative teams at iDream consist of writers and directors, actors, editors, camera, social media and content teams that work in perfect symphony to entertain and amuse you, every single day.</p>
                        </div>
                    </div>
                </div>
                <!-- web series rows -->
                <div class="container">


                    @if(sizeof($webseries)>0)
                      <?php
                            $i=0;
                      ?>
                        @foreach($webseries as $webseries)
                              @if($i % 2 == 0)
                    <div class="row py-4 wow fadeInUp">
                        <div class="col-md-6 webimgcolumn">
                            <figure>
                                <a href="https://www.youtube.com/channel/UCNmbqDwWcKvXxrF7a4bIpYA/channels" target="_blank"><img src="/uploads/webseries/{{$webseries->w_banner}}" class="img-fluid"> </a>
                            </figure>
                        </div>
                        <div class="col-md-6 align-self-center text-xs-center"> <img class="img-ch" src="/uploads/webseries/{{$webseries->w_logo}}">
                            <h5 class="h5 fbold pt-3"><a href="{{ $webseries->w_link }}" target="_blank" class="fblack">{{ $webseries->w_title }}</a></h5>
                            <ul class="list-inline weblist">
                                <li class="list-inline-item"><a href="{{ $webseries->w_link }}" target="_blank">View More</a></li>
                            </ul>
                            <p class="text-justify py-4 text-xs-center">{{ $webseries->w_description }}</p>
                        </div>
                    </div>
                              @else
                    <div class="row py-4 wow fadeInUp">
                        <div class="col-md-6 align-self-center text-xs-center"> <img class="img-ch" src="/uploads/webseries/{{$webseries->w_logo}}">
                            <h5 class="h5 fbold pt-3"><a href="{{ $webseries->w_link }}" target="_blank" class="fblack">{{ $webseries->w_title }}</a></h5>
                            <ul class="list-inline weblist">
                                <li class="list-inline-item"><a href="{{ $webseries->w_link }}" target="_blank">View More</a></li>
                            </ul>
                            <p class="text-justify py-4 text-xs-center">{{ $webseries->w_description }}</p>
                        </div>
                        <div class="col-md-6 webimgcolumn">
                            <figure>
                                <a href="https://www.youtube.com/channel/UC3t8NHsv6GELrIwOaINqW8Q/videos" target="_blank"><img src="/uploads/webseries/{{$webseries->w_banner}}" class="img-fluid"></a>
                            </figure>
                        </div>
                    </div>
                              @endif
                                  <?php
                                  $i++;
                                  ?>
                        @endforeach

                    @endif

                </div>
                <!--/ web series rows -->
            </section>
        </section>
        <!--/ sub page body -->
    </section>




    <!--/ main -->@endsection