@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">Partner </span> <span class="flight">Channels</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Original Content</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('partner-channels')}}" class="fblue">Partner Channels</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <!-- what we provide in social media management -->
            <section class="clients-bg">
                <div class="container">
                    <div class="row flex-row align-items-center">
                        <div class="col-md-12 text-center col-12">
                            <article class="">
                                <h1 class="h1 flight fwhite">Partner channels are creators who rely on our content and social media teams to help reach out to their audience. </h1>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection