@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->


    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h2 class="h2 text-uppercase fwhite"><span class="fbold">Entertainment </span> <span class="flight">Series</span></h2>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Original Content</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('entertainments')}}" class="fblue">Entertainment Series </a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <!-- what we provide in social media management -->
            <section class="soundseervices py-4">
                <div class="container">
                    <!-- row -->
                    <div class="row py-3 wow fadeInUp">
                        <div class="col-md-12 text-xs-center">
                            <article class="subsectiontitle mt-2">
                                <h4 class="h4 fbold m-0">iDream <span class="fblue">Entertainment</span></h4>
                                <p>Movies, Music and More</p>
                            </article>
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!-- web series rows -->
                <div class="container">
                    <div class="row wow fadeInUp">

                        @if(sizeof($entertainments)>0)
                            @foreach($entertainments as $entertainment)
                        <div class="col-md-4 text-center w-50-device text-sm-center">
                            <div class="colblock p-3">
                                <a href="https://www.youtube.com/user/idreammovies/featured" target="_blank"> <img class="img-ch" src="/uploads/entertinement/{{$entertainment->ent_logo}}"> </a>
                                <h5 class="h5 fbold pt-3"><a href="{{$entertainment->ent_link}}" target="_blank" class="fblack">{{$entertainment->ent_title}}</a></h5>
                                <ul class="list-inline weblist">
                                    <li class="list-inline-item"><a href="{{$entertainment->ent_link}}" target="_blank">View More</a></li>
                                </ul>
                                <p class="py-2 text-sm-center">{{$entertainment->ent_description}}</p>
                            </div>
                        </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <!--/ web series rows -->
            </section>
        </section>
        <!--/ sub page body -->
    </section>




    <!--/ main -->@endsection