@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 fwhite"><span class="fbold">iDREAM </span> <span class="flight">POST</span></h1> </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Our Products</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('idreampost')}}" class="fblue">iDream Post</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
    <article class="subsectiontitle mt-2">
        <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
        <p>Aenean suscipit eget mi act</p>
    </article>
    title -->
        <section class="subpage-body py-4">
            <!-- what we provide in social media management -->
            <section class="idpostintro py-4">
                <div class="container">
                    <div class="row pb-3 wow fadeInUp">
                        <div class="col-md-10 offset-md-1 text-center py-3">
                            <h4 class="flight h4">Does your heart crave for more and more news??</h4>
                            <p class="pb-2">iDreammovies works towards bringing to you the happenings from the media. Since its inception, the writers have been striving to write quality content, that is accurate and reliable. We hold a firm belief in value added journalism. Political News and Entertainment news are presented to you in the most innovative way possible. Tollywood, Bollywood, Kollywood, Mollywood, Hollywood, Sandalwood, you name it, we have it!</p> <a href="http://www.idreampost.com" class="btn btn-warning d-inline-block mt-2" target="_blank">Visit Site</a> </div>
                    </div>
                </div>
            </section>
            <div class="container chanelscolumn">
                <div class="row py-3 wow fadeInUp">
                    <div class="col-md-4 text-center">
                        <div class="colblock2 p-3">
                            <a href="http://www.idreampost.com/news/movies" target="_blank"> <img class="img-ch" src="{{URL::asset('frontend/img/idposticon01.jpg')}}"> </a>
                            <h6 class="h6 fbold pt-3"><a href="javascript:void(0)" class="fblack">Movies News</a></h6>
                            <ul class="list-inline weblist">
                                <li class="list-inline-item"><a href="http://www.idreampost.com/news/movies" target="_blank">View More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="colblock2 p-3">
                            <a href="http://www.idreampost.com/news/political" target="_blank"> <img class="img-ch" src="{{URL::asset('frontend/img/idposticon02.jpg')}}"> </a>
                            <h6 class="h6 fbold pt-3"><a href="javascript:void(0)" class="fblack">Political News</a></h6>
                            <ul class="list-inline weblist">
                                <li class="list-inline-item"><a href="http://www.idreampost.com/news/political" target="_blank">View More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="colblock2 p-3">
                            <a href="http://www.idreampost.com/news/events" target="_blank"> <img class="img-ch" src="{{URL::asset('frontend/img/idposticon03.jpg')}}"> </a>
                            <h6 class="h6 fbold pt-3"><a href="javascript:void(0)" class="fblack">Events News</a></h6>
                            <ul class="list-inline weblist">
                                <li class="list-inline-item"><a href="http://www.idreampost.com/news/events" target="_blank">View More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="colblock2 p-3">
                            <a href="http://www.idreampost.com/news/nri" target="_blank"> <img class="img-ch" src="{{URL::asset('frontend/img/idposticon04.jpg')}}"> </a>
                            <h6 class="h6 fbold pt-3"><a href="javascript:void(0)" class="fblack">NRI News</a></h6>
                            <ul class="list-inline weblist">
                                <li class="list-inline-item"><a href="http://www.idreampost.com/news/nri" target="_blank">View More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="colblock2 p-3">
                            <a href="http://www.idreampost.com/show-times" target="_blank"> <img class="img-ch" src="{{URL::asset('frontend/img/idposticon05.jpg')}}"> </a>
                            <h6 class="h6 fbold pt-3"><a href="javascript:void(0)" class="fblack">Show Times</a></h6>
                            <ul class="list-inline weblist">
                                <li class="list-inline-item"><a href="v" target="_blank">View More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="colblock2 p-3">
                            <a href="http://www.idreampost.com/spotlight" target="_blank"> <img class="img-ch" src="{{URL::asset('frontend/img/idposticon06.jpg')}}"> </a>
                            <h6 class="h6 fbold pt-3"><a href="javascript:void(0)" class="fblack">Spotlight</a></h6>
                            <ul class="list-inline weblist">
                                <li class="list-inline-item"><a href="http://www.idreampost.com/spotlight" target="_blank">View More</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection