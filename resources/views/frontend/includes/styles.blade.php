<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CLato:300,400" rel="stylesheet">

<!-- Bootstrap -->
<link type="text/css" rel="stylesheet" href="{{URL::asset('frontend/css/bootstrap.min.css')}}"/>

<!-- Owl Carousel -->
<link type="text/css" rel="stylesheet" href="{{URL::asset('frontend/css/owl.carousel.css')}}" />
<link type="text/css" rel="stylesheet" href="{{URL::asset('frontend/css/owl.theme.default.css')}}" />

<!-- Font Awesome Icon -->
<link rel="stylesheet" href="{{URL::asset('frontend/css/font-awesome.min.css')}}">

<!-- Custom stlylesheet -->
<link type="text/css" rel="stylesheet" href="{{URL::asset('frontend/css/style.css')}}"/>