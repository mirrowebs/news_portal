

<footer id="footer">


    <!-- Bottom Footer -->
    <div id="bottom-footer" class="section">
        <!-- CONTAINER -->
        <div class="container">
            <!-- ROW -->
            <div class="row">
                <!-- footer links -->
                <div class="col-md-6 col-md-push-6">
                    <ul class="footer-links">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Advertisement</a></li>
                        <li><a href="#">Privacy</a></li>
                    </ul>
                </div>
                <!-- /footer links -->

                <!-- footer copyright -->
                <div class="col-md-6 col-md-pull-6">
                    <div class="footer-copyright">
								<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
                    </div>
                </div>
                <!-- /footer copyright -->
            </div>
            <!-- /ROW -->
        </div>
        <!-- /CONTAINER -->
    </div>
    <!-- /Bottom Footer -->
</footer>
<!-- /FOOTER -->

<!-- Back to top -->
<div id="back-to-top"></div>
<!-- Back to top -->

<!-- jQuery Plugins -->
<script src="{{URL::asset('frontend/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('frontend/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{URL::asset('frontend/js/main.js')}}"></script>
<!--/ footer -->


