@extends('frontend.layouts.app') @section('title', $title) @section('content')

    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">Terms of </span> <span class="flight">Services</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('terms-conditions')}}" class="fblue">Terms of Services</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-justify pb-3">iDream Media distributes movies and other filmed entertainment and reserves the right to display and promote the filmed entertainment through Youtube channels, website and user interfaces like Apps. Under no circumstances shall iDream Media, or its shareholders, directors, officers or employees or licensors be held liable for any loss or damage caused by your reliance on information obtained through our service. It is your responsibility to evaluate the information, opinion, advice, or other content available through the our service.</p>
                        <p class="text-justify pb-3">The use of the iDream Media Services and access to the Content are subject to you having the necessary hardware, software and network resources.</p>
                        <p class="text-justify pb-3">You may access the iDream Media Services and Content only in geographic locations where we offer our services.</p>
                        <p class="text-justify pb-3">We are not responsible for any costs that you incur while attempting to access the iDream Media Services or the Content, including payments towards Internet and broadband access or any additional expenses incurred by you including charges imposed by your network service provider for 3G/or roaming access.</p>
                        <p class="text-justify pb-3">We reserve the right to terminate or restrict your use of our service, without notice, for any or no reason whatsoever.</p>
                    </div>
                </div>
            </div>
        </section>
        <!--/ sub page body -->
    </section>



    <!--/ main -->@endsection