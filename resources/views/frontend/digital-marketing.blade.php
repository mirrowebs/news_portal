@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h2 class="h2 text-uppercase fwhite"><span class="fbold">Digital </span> <span class="flight">Marketing</span></h2> </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="index.php" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Services</a></li>
                            <li class="breadcrumb-item active float-left"><a href="javascript:void(0)" class="fblue">Digital Marketing</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
    <article class="subsectiontitle mt-2">
        <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
        <p>Aenean suscipit eget mi act</p>
    </article>
    title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <figure><img src="{{URL::asset('frontend/img/digital-marketing.png')}}" class="img-fluid"></figure>
                    </div>
                    <div class="col-md-6">
                        <h5 class="h5 fbold py-2">We Offer a Full Range of Digital Marketing Services!</h5>
                        <p class="text-justify pb-3">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
                        <p class="text-justify pb-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium non optio, consectetur iusto omnis obcaecati quibusdam delectus reprehenderit dignissimos rem ab, a ad debitis asperiores corporis porro consequatur at laudantium repudiandae. Nesciunt.</p>
                        <ul class="py-4 listitems">
                            <li>Qolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh;</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati ex numquam earum quis, expedita voluptas. Nihil corporis dolor, voluptas reprehenderit aspernatur quo ut. </li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi cupiditate, pariatur unde officiis quae voluptatibus.</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection