@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">Social Media </span> <span class="flight">Management</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Services</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('socialmedia-management')}}" class="fblue">Social Media Management</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-md-6">
                        <h5 class="h5 fbold py-2">Your Complete Solutions for Social Media Management</h5>
                        <p class="text-justify pb-3">We offer social media management services. Transform your social media presence with high-quality content, daily activity, and increasing followers. Our team understands Facebook, Instagram, Google and YouTube Marketing services and how to optimise your social media channels to satisfy your marketing objectives. </p>
                        <p class="text-justify pb-3">We are on social platforms to enhance your image with our expertise to help strike the right chord at the right time. Safeguarding the Social Media presence of our celebrities and brands, taking care of security issues of the pages and protecting their clean image are all part of our Social Media and Content Management.</p>
                    </div>
                    <div class="col-md-6">
                        <figure><img src="{{URL::asset('frontend/img/socialmedia-icon.png')}}" class="img-fluid"></figure>
                    </div>
                </div>
            </div>
            <!-- what we provide in social media management -->
            <section class="social-provide py-4">
                <div class="container">
                    <div class="row py-3 wow fadeInUp">
                        <div class="col-md-12 text-xs-center">
                            <article class="subsectiontitle mt-2 ">
                                <h4 class="h4 fbold m-0">What <span class="fblue">we provide</span></h4>
                                <p>Services in Social Media Management </p>
                            </article>
                        </div>
                    </div>
                    <div class="row py-4 wow fadeInUp">
                        <div class="col-md-6 text-xs-center">
                            <figure><img src="{{URL::asset('frontend/img/socialicon-01.png')}}"></figure>
                            <h6 class="h6 py-2 fbold">Brand and Fans</h6>
                            <p class="text-justify text-xs-center pb-xs-15">We will act as a bridge between the brand/celebrity in interacting with their fans or target audience and maintain a relationship.</p>
                        </div>
                        <div class="col-md-6 text-xs-center">
                            <figure><img src="{{URL::asset('frontend/img/socialicon-04.png')}}"></figure>
                            <h6 class="h6 py-2 fbold">Brand and Content</h6>
                            <p class="text-justify text-xs-center pb-xs-15">Our team helps in creating and managing Brand related content which helps it to stand tall in this competitive market.</p>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ what we provide in social media management -->
            <!-- why need social media management-->
            <section class="yneedsocial py-3">
                <div class="container">
                    <div class="row py-4 wow fadeInUp">
                        <div class="col-md-12">
                            <!-- contact form for social media management-->
                            <div class="form-social p-3">
                                <div class="row pt-1">
                                    <div class="col-md-12">
                                        <h5 class="h5 fbold py-2">Contact for Social Media Management</h5>
                                    </div>
                                </div>
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                                <form method="POST" id="socialmedia_validation" action="{{route('socialmedia-management')}}" accept-charset="UTF-8" enctype="multipart/form-data" >
                                    {{ csrf_field() }}
                                    <div class="row py-1">
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_website') ? 'has-error' : ''}}">
                                                <label>Website</label>
                                                <input type="text" class="form-control" name="cs_website" id="cs_website" >
                                                {!! $errors->first('cs_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_name') ? 'has-error' : ''}}">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="cs_name" id="cs_name" >
                                                {!! $errors->first('cs_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row py-1">
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_email') ? 'has-error' : ''}}">
                                                <label>E-mail</label>
                                                <input type="text" class="form-control" name="cs_email" id="cs_email" >
                                                {!! $errors->first('cs_email', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_contact_number') ? 'has-error' : ''}}">
                                                <label>Phone</label>
                                                <input type="text" class="form-control" name="cs_contact_number" id="cs_contact_number" >
                                                {!! $errors->first('cs_contact_number', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row py-1">
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_company_name') ? 'has-error' : ''}}">
                                                <label>Company name</label>
                                                <input type="text" class="form-control" name="cs_company_name" id="cs_company_name" >
                                                {!! $errors->first('cs_company_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('cs_location') ? 'has-error' : ''}}">
                                                <label>Location</label>
                                                <input type="text" class="form-control" name="cs_location" id="cs_location" >
                                                {!! $errors->first('cs_location', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row py-1">
                                        <div class="col-md-12 {{ $errors->has('cs_message') ? 'has-error' : ''}}">
                                            <label>Put Your Ideas</label>
                                            <textarea rows="4" cols="50" name="cs_message" id="cs_message" > </textarea>
                                            {!! $errors->first('cs_message', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="row py-1">
                                        <div class="col-md-12">
                                            <button class="btn btn-block btn-success" type="submit">SUBMIT</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--/ contact formfor social media management-->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ why need social media management-->
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection