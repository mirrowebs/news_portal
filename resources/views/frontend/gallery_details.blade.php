@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 fwhite"><span class="fbold">iDREAM </span> <span class="flight">GALLERY</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="{{route('gallery')}}" class="fwhite">Gallery</a></li>
                            <li class="breadcrumb-item active float-left"><a href="javascript:void(0)" class="fblue">{{ $galleryInfo->g_title }}</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <!-- image gallery -->
            <section class="gallery-img py-2">
                <div class="container">
                    <div class="row py-3 wow fadeInUp">
                        <article class="subsectiontitle my-4">
                            <h4 class="h4 fbold m-0"><span class="fblue">{{ $galleryInfo->g_title }}</span></h4>
                            <p>Gallery</p>
                        </article>
                    </div>
                </div>
                <div class="wow fadeInUp" data-nanogallery2>
                    @foreach($gallery as $gal)
                    <a href="/uploads/gallery/{{$gal->gi_image}}" data-ngThumb="/uploads/gallery/thumbs/{{$gal->gi_image}}"></a>
                    @endforeach
                </div>
            </section>
            <!--/ image gallery -->
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection