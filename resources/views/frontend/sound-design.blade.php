@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h2 class="h2 text-uppercase fwhite"><span class="fbold">Sound </span> <span class="flight">Design</span></h2>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="index.php" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Services</a></li>
                            <li class="breadcrumb-item active float-left"><a href="javascript:void(0)" class="fblue">Sound Design</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row bluebox">
                    <div class="col-md-6 p-0 soundimg"> </div>
                    <div class="col-md-6 align-self-center">
                        <article class="p-5">
                            <h5 class="h5 fbold pt-2 fwhite">About Recordings</h5>
                            <p class="text-justify pb-5 fwhite">Our repair services encompass everything from seamless glass repair for your cracked iPhone glass to complete functional restoration of your water damaged iPad or Macbook. We carry an extremely high success rate on even the most severe water damaged phones, computers, and even tablets.</p>
                            <h5 class="h5 fbold pt-2 fwhite">We are the best</h5>
                            <p class="text-justify pt-2 fwhite">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et</p>
                        </article>
                    </div>
                </div>
            </div>
            <!-- sound Statistics-->
            <section class="soundstats py-5 mt-5">
                <div class="">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2 class="h2 sectiontitle d-inline fwhite">Our Sound <span class="fbold">Statistics</span></h2>
                            </div>
                        </div>
                        <div class="row py-5">
                            <div class="col-md-3 text-center">
                                <h1 class="h1 fwhite fbold">256</h1>
                                <h4 class="fbold fwhite text-uppercase">Movies</h4>
                            </div>
                            <div class="col-md-3  text-center">
                                <h1 class="h1 fwhite fbold">729</h1>
                                <h4 class="fbold fwhite text-uppercase">Short Films</h4>
                            </div>
                            <div class="col-md-3  text-center">
                                <h1 class="h1 fwhite fbold">650</h1>
                                <h4 class="fbold fwhite text-uppercase">Interviews</h4>
                            </div>
                            <div class="col-md-3 text-center">
                                <h1 class="h1 fwhite fbold">1200</h1>
                                <h4 class="fbold fwhite text-uppercase">Songs Composing</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sound Stastics -->
            <!-- what we provide in social media management -->
            <section class="soundseervices py-4">
                <div class="container">
                    <div class="row py-3">
                        <div class="col-md-12">
                            <article class="subsectiontitle mt-2">
                                <h4 class="h4 fbold m-0">Our Sound <span class="fblue">Services</span></h4>
                                <p>Services we prvide in Sound Management</p>
                            </article>
                        </div>
                    </div>
                    <div class="row py-4">
                        <div class="col-md-4">
                            <figure><img src="{{URL::asset('frontend/img/socialicon-01.png')}}"></figure>
                            <h6 class="h6 py-2 fbold">Recording and Mastering</h6>
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
                        </div>
                        <div class="col-md-4">
                            <figure><img src="{{URL::asset('frontend/img/socialicon-02.png')}}"></figure>
                            <h6 class="h6 py-2 fbold">Audio and Video Production</h6>
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
                        </div>
                        <div class="col-md-4">
                            <figure><img src="{{URL::asset('frontend/img/socialicon-03.png')}}"></figure>
                            <h6 class="h6 py-2 fbold">Full Pack Promotion</h6>
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem dummy text ever since the</p>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ what we provide in social media management -->
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection