@extends('frontend.layouts.app') @section('title', $title) @section('content')

    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h2 class="h2 fwhite"><span class="fbold">ABOUT </span> <span class="flight">iDREAM</span></h2>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('aboutus')}}" class="fblue">About</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <section class="subpage-body py-5">
            <div class="container">
                <div class="row wow fadeInUp pb-4">
                    <div class="col-md-6 w-100-device text-sm-center">
                        <figure><img src="{{URL::asset('frontend/img/aboutimg01.jpg')}}" class="img-fluid"></figure>
                    </div>
                    <div class="col-md-6 w-100-device">
                        <h5 class="h5 fbold py-2">Welcome to iDream Media</h5>
                        <p class="text-justify pb-3">Our journey at iDream Media started with showcasing our extensive content library including 2500+ movies, quickly adding audio launches, movie functions and other live events. Our long form interviews started a trend, pushing the boundaries of journalism and adding intrinsic value to pop culture conversations.</p>
                        <p class="text-justify pb-3">Soon after we started our original content and soon enough that started spreading smiles. We havenâ€™t looked back since, with our creative teams comprising of writers, directors, editors, actors, camera teams, design, content and social media teams to bring to life the stories and series we watch and enjoy in many different channels. From YouTube, we went on to do Television and now to making our presence felt on OTT platforms and this journey is only getting bigger, better and stronger.</p>
                        <p class="text-justify pb-3">iDream Movies to iDream News, our Events in USA, and the 250+ channels owned by us are a call to the future, in terms of content, entertainment, vision, narrative and formats. </p>
                    </div>
                </div>
            </div>
            <!-- ceo row -->
            <section class="chairman-section py-5">
                <div class="container">
                    <div class="row wow fadeInUp">
                        <div class="col-md-12 text-xs-center">
                            <article class="subsectiontitle mt-2">
                                <h4 class="h4 fbold m-0">Chinna <span class="fblue">Vasudeva Reddy</span></h4>
                                <p>Founder</p>
                            </article>
                        </div>
                    </div>
                    <div class="row pt-4 wow fadeInUp">
                        <div class="col-md-3">
                            <figure class="picfounder"> <img src="{{URL::asset('frontend/img/ceopic.jpg')}}" class="img-fluid"> </figure>
                        </div>
                        <div class="col-md-9">
                            <p class="text-justify pb-3">From being the Principal Engineer at United Online to producing films at KAD entertainment, Vasudeva Reddy has dabbled in Technology, Entertainment, Business and Politics. iDream Media has been all about realising â€˜Dreamsâ€™ in the Digital universe, starting with the first long form interviews in South India, a concept and format that has not only been a huge success in terms of numbers but helped in being a chronicler of times, and added immense journalistic value to our current conversations.</p>
                            <p class="text-justify pb-3"> Being in key positions in Telugu associations in North America to organising huge concerts and events regularly in New York and New Jersey has helped create and sustain the togetherness of a large Telugu community in the US. From being the top distributor in the overseas market for Telugu cinema to amassing the digital rights to thousands of Telugu movies, CVRâ€™s journey is as interesting as itâ€™s prolific. He uses the discipline and drive from 12 years of Silicon Valley experience with the people skills it takes to successfully run the New Jersey Telugu Association in the rapidly changing and evolving Telugu cinema and digital industry.</p>
                            <p class="text-justify pb-3">iDream Media and Vasudeva Reddy have been instrumental in being a strong bridge between the Telugu community in the Telugu states and the NRI Telugu community. This is through many major executive positions in Telugu community groups such as TANA and also the Vice President of TFAS (Telugu Fine Arts Society). As a partner in KAD entertainment, he was instrumental in taking overseas distribution to where it is now, by distributing more than 100 Films including Tagore, Nuvvosthanante Nenoddantana, Happy, Arjun, Sri Ramadasu, Anand, Godavari, Jai Chiranjeeva, Stalin and others.</p>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ ceo row -->
        </section>
        <!--/ sub page body -->
    </section>



    <!--/ main -->@endsection