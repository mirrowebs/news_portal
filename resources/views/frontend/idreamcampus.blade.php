@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->
    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 fwhite"><span class="fbold">iDREAM </span> <span class="flight">CAMPUS</span></h1> </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item float-left "><a href="javascript:void(0)" class="fwhite">Our Products</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('idreamcampus')}}" class="fblue">iDream Campus</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
    <article class="subsectiontitle mt-2">
        <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
        <p>Aenean suscipit eget mi act</p>
    </article>
    title -->
        <section class="subpage-body py-4">
            <!-- what we provide in social media management -->
            <section class="soundseervices py-4">
                <div class="container">
                    <div class="row pb-3 wow fadeInUp">
                        <div class="col-md-10 offset-md-1 text-center py-3">
                            <h4 class="flight h4">The iDream Campus is a film school like no other, focussed on moulding the content, the creator and vision of student filmmaker to give and enrich the entertainment industries.</h4> <a href="http://www.idreamcampus.com/" target="_blank" class="btn btn-warning d-inline-block mt-2">View Site</a> </div>
                        <div class="col-md-6"> <img src="{{URL::asset('frontend/img/campusimg02.jpg')}}" class="img-fluid"> </div>
                        <div class="col-md-6 align-self-center">
                            <p class="text-justify py-2">iDream has been in the business of creating original content by young and talented newbies in the industry, distributing huge projects as well as quality cinema overseas and accessing digital rights for classics as well as the latest new movies. Founded in 2013 by Chinna Vasudeva Reddy, iDream is a company to reckon with on YouTube, with more than 250+ channels under it, more than 5 Million subscribers and 5 Billion views.</p>
                            <h4 class="h4 fbold py-2">Our Vision</h4>
                            <p class="text-justify pb-2">iDream Campus is the home of innovative, creative education in filmmaking, with an emphasis on story. We guide, instruct and empower our students to conceive and develop their story ideas, bringing them to life. We believe powerful stories, given a platform, can lift the human condition.</p>
                            <p class="text-justify pb-2">iDream Campus hopes to give back to the film and television industry and the society; an institutional frame work for the formal training of directors, cinematographers, designers, technicians and actors who will enhance the standards of Telugu cinema and television.</p>
                            <p class="text-justify pb-2">Our final vision is to build a globally acclaimed comprehensive learning platform for those who dream to fly high in the universe of moving images.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="idfacilities">
                <div class="container">
                    <div class="flex-row row align-items-center wow fadeInUp">
                        <div class="col-md-6 text-right text-xs-center">
                            <h4 class="h4 fbold fwhite py-2">Short Courses</h4>
                            <p class="fwhite text-xs-center">Our short courses are a practical way for kids during the summer as well as working professionals in any industry to upgrade their skill set. It is a great way to keep up with changing technologies and to learn new skills that can help their professional development. For non-media professionals, For non-media professionals, it is a way to learn the basics of art forms they have always admired and wondered about the magic that goes on behind-the-scenes.</p>
                        </div>
                        <div class="col-md-6 text-xs-center">
                            <h4 class="h4 fbold fwhite py-2" style="margin-top:-16px;">Facilities at iDream</h4>
                            <p class="fwhite text-xs-center">From cameras and equipment to a state-of-the-art Dubbing studio and editing facilities, iDream Campus comes loaded with every Film Student's dream campus facilities. To create good content, the key is to work with a strong network of experienced professionals and provide the right facilities. We set up teams to shape your idea, push the execution and manage the workflow from pre- to post-production.</p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="row py-4 wow fadeInUp campus-columns">
                    <div class="col-md-4">
                        <figure><img src="{{URL::asset('frontend/img/campusicon01.png')}}"></figure>
                        <h6 class="h6 py-2 fbold">Production Level Teaching</h6>
                        <p class="text-justify">What separates iDream from other filmmaking courses is the hands on experience students get from actually working on projects and watching industry professionals work and interact with them. Students are now ready to create a professional production with our guidance.</p>
                    </div>
                    <div class="col-md-4">
                        <figure><img src="{{URL::asset('frontend/img/campusicon02.png')}}"></figure>
                        <h6 class="h6 py-2 fbold">Teaching by Industry Experts</h6>
                        <p class="text-justify">iDream faculty are not just teachers, they are doers. They've been in the industry and are simply here to share and work with young budding filmmakers and impart the knowledge and experience they got with years and a number of projects in their particular field of expertise.</p>
                    </div>
                    <div class="col-md-4">
                        <figure><img src="{{URL::asset('frontend/img/campusicon03.png')}}"></figure>
                        <h6 class="h6 py-2 fbold">Top level Facilities</h6>
                        <p class="text-justify">The equipment and facilities at iDream for the student filmmakers help them create the right project. The softwares, the teaching, the standards as well as the work ethic imparted by our faculty is local in its flavour and global in its approach.</p>
                    </div>
                </div>
            </div>
        </section>
        <!--/ sub page body -->
    </section>
    <!--/ main -->@endsection