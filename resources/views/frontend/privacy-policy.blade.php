@extends('frontend.layouts.app') @section('title', $title) @section('content')

    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 text-uppercase fwhite"><span class="fbold">Privacy </span> <span class="flight">Policy</span></h1> </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('privacy-policy')}}" class="fblue">Privacy policy</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
    <article class="subsectiontitle mt-2">
        <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
        <p>Aenean suscipit eget mi act</p>
    </article>
    title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-justify pb-3">iDream Media values your privacy. This Privacy Policy describes the types of personal information we collect, the way it is used and shared, and the choices you can make about our collection, use and disclosure of the information. We ensure to protect the security of the information</p>
                        <h5 class="h5 fbold">Information We Collect</h5>
                        <p class="text-justify pb-3">iDream Media may obtain personal information about you from sources like our website. We may collect information that you need to fill out while accessing services of our website.</p>
                        <h5 class="h5 fbold">Types of Information</h5>
                        <p class="text-justify pb-3">When you visit and interact with our website, certain information may be collected via a cookie, including:</p>
                        <p class="text-justify pb-3">your computer's Internet Protocol (IP) address; your browser type and operating system; search terms that you use to reach our site; the web pages you were visiting immediately before and after you came to our site; dates and times of visits to our site; information on actions taken on our site (such as page views and site navigation patterns); standard server log information; and/or advertisement interaction and viewing data, such as ad click-through rates and information about how many times you viewed a particular ad. Please note that iDream Media website may contain links to other websites, including social networking sites.</p>
                        <h5 class="h5 fbold">Information Usage:</h5>
                        <p class="text-justify pb-3">We would use the information provided by you to:</p>
                        <p class="text-justify pb-3">Improve our website Enable us to provide you the most user-friendly experience. Improve and customize our services, content and other commercial /non - commercial features on the website; Send you newsletters about our services Comply with any court judgment / decree / order / directive / legal & government authority /applicable law; Investigate potential violations or applicable national & international laws; Investigate deliberate damage to the website/services or its legitimate operation; Detect, prevent, or otherwise address security and/or technical issues; Respond to Claims of third parties; We reserve the right to disclose your personally identifiable information as required by law and when we believe that disclosure is necessary to protect our rights and/or to comply with a judicial proceeding, court order, or legal process served on our website.</p>
                        <h5 class="h5 fbold">Business Communications</h5>
                        <p class="text-justify pb-3">You may provide your email address to us or other contact information to receive industry news, intelligence, information about iDream Media services, and other business communications. You may opt out of our email marketing communications using the link at the bottom of each email.</p>
                        <h5 class="h5 fbold">Information Security</h5>
                        <p class="text-justify pb-3">iDream Media has technical, administrative and physical safeguards in place to help protect against unauthorized access to, use or disclosure of user information we maintain. Although we work hard to protect personal information that we collect and store, no program is one hundred percent secure and we cannot guarantee that our safeguards will prevent every unauthorized attempt to access, use or disclose personal information. If you become aware of a security issue, please email us. We will work with you to address such problems</p>
                        <p class="text-justify pb-3">Changes in this Privacy Statement When we change our privacy policy, we will post those changes to this privacy statement and such other places we deem appropriate so that you are updated about the manner we collect information. In case of queries feel free to drop a mail to hr@idream.email or send us a letter addressed to iDream Media, Sagar society, Banjara Hills.</p>
                    </div>
                </div>
            </div>
        </section>
        <!--/ sub page body -->
    </section>



    <!--/ main -->@endsection