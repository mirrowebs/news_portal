@extends('frontend.layouts.app') @section('title', $title) @section('content')
    <!-- main-->





    <section class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <h1 class="h2 fwhite"><span class="fbold">iDREAM </span> <span class="flight">EVENTS</span></h1>
                        </article>
                    </div>
                    <div class="col-md-6 text-right pt-2">
                        <ol class="brcrumb float-right">
                            <li class="breadcrumb-item float-left "><a href="{{route('home')}}" class="fwhite">Home</a></li>
                            <li class="breadcrumb-item active float-left"><a href="{{route('events')}}" class="fblue">iDream Events</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->
        <!-- sub page body -->
        <!-- title
        <article class="subsectiontitle mt-2">
            <h4 class="h4 fbold m-0">Our <span class="fblue">Mission</span></h4>
            <p>Aenean suscipit eget mi act</p>
        </article>
        title -->
        <section class="subpage-body py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <article class="subsectiontitle mt-2">
                            <h4 class="h4 fbold m-0">Recent <span class="fblue">Events</span></h4>
                            <p>Recent Events completed, and upcoming</p>
                        </article>
                    </div>
                </div>
                <!-- recent events columns -->
                <div class="row py-4">
                    @if(sizeof($events)>0)
                        @foreach($events as $event)
                    <div class="col-md-3 eventcolumn">
                        <a href="{{route('event',['id' => $event->e_id,'alias'=>$event->e_alias])}}"><img src="/uploads/events/thumbs/{{$event->e_image}}" class="img-fluid eventpic object-fit_cover"></a>
                        <article class="align-self-center">
                            <h6 class=""><a href="{{route('event',['id' => $event->e_id,'alias'=>$event->e_alias])}}" class="fblack">{{$event->e_title}}</a></h6>
                            <ul class="list-inline py-1 eventlist" style="line-height: 20px;>
                                <li class="list-inline-item "><i class="fa fa-calendar fblue" style="padding: 3px" aria-hidden="true"></i>{{$event->e_schedule_info}}</li>
                                <li class="list-inline-item "><i class="fa fa-map-marker fblue" style="padding: 3px" aria-hidden="true"></i> {{$event->e_place}}</li>
                            </ul>
                            <ul class="list-inline pt-4 eventsanchor">
                                <li class="list-inline-item "> <a href="{{route('event',['id' => $event->e_id,'alias'=>$event->e_alias])}}" class="d-inline p-2"><i class="fa fa-file-text" aria-hidden="true"></i> INFORMATION</a> </li>
                                {{--<li class="list-inline-item "> <a href="http://www.jerseytelugu.org/event/49" class="d-inline p-2"><i class="fa fa-ticket" aria-hidden="true"></i> BUY TICKET</a> </li>--}}
                            </ul>
                        </article>
                    </div>
                        @endforeach
                    @else
                        <div class="row" align="center"> There is no Events yet </div>
                    @endif
                </div>
                <!--/ recent events columns -->
            </div>
            <!-- what we provide in social media management -->
            <section class="eventsummary my-4">
                <div class="container">
                    <div class="row py-3 wow fadeInUp">
                        <div class="col-md-12 text-xs-center">
                            <article class="subsectiontitle mt-2">
                                <h4 class="h4 fbold m-0">Our Event <span class="fblue">Summary</span></h4>
                                <p>Aenean suscipit eget mi act</p>
                            </article>
                        </div>
                    </div>
                    <div class="row py-4 wow fadeInUp">
                        <div class="col-md-4 text-center mb-3">
                            <figure><img src="{{URL::asset('frontend/img/eventsummaryicon01.png')}}"></figure>
                            <h6 class="h6 py-2 ">12 thousand adoring fans</h6>
                        </div>
                        <div class="col-md-4 text-center mb-3">
                            <figure><img src="{{URL::asset('frontend/img/eventsummaryicon02.png')}}"></figure>
                            <h6 class="h6 py-2 ">New Jersey Prudential Centre</h6>
                        </div>
                        <div class="col-md-4 text-center mb-3">
                            <figure><img src="{{URL::asset('frontend/img/eventsummaryicon03.png')}}"></figure>
                            <h6 class="h6 py-2 ">Chitra, SP Balasubrahmanyam, Karthik, Yuvan Shankar raja and others</h6>
                        </div>
                        <div class="col-md-4 text-center mb-3">
                            <figure><img src="{{URL::asset('frontend/img/eventsummaryicon04.png')}}"></figure>
                            <h6 class="h6 py-2 ">Telugu-Tamil event</h6>
                        </div>
                        <div class="col-md-4 text-center mb-3">
                            <figure><img src="{{URL::asset('frontend/img/eventsummaryicon05.png')}}"></figure>
                            <h6 class="h6 py-2 ">1st South Indian concert of that scale in US</h6>
                        </div>
                        <div class="col-md-4 text-center mb-3">
                            <figure><img src="{{URL::asset('frontend/img/eventsummaryicon06.png')}}"></figure>
                            <h6 class="h6 py-2 ">Ilaiyaraajaâ€™s first North American concert</h6>
                        </div>
                    </div>
                    <!-- events description -->
                    <div class="row py-3">
                        <div class="col-md-6">
                            <p class="text-justify py-2"> From concerts by Iliayaraaja, Devi Sri Prasad, Mani Sharma to events with Dil Raju, Nikhil Sidhartha, Madhu Shalini and many others. Handling all aspects of organizing and executing huge star-studded events is what we specialise in. </p>
                            <p class="text-justify py-2"> iDream Media and Vasudeva Reddy have been instrumental in being a strong bridge between the Telugu community in the Telugu states and the NRI Telugu community. This is through many major executive positions in Telugu community groups such as TANA (Telugu Association of North America) for 8 years such as Regional Vice President, Joint Secretary and Foundation Trustee. He is also the Vice President of TFAS (Telugu Fine Arts Society). </p>
                        </div>
                        <div class="col-md-6">
                            <p class="text-justify py-2"> Hear's also the Founder-President of NJTA (New Jersey Telugu Association) started in 2016. He started it as a cultural bridge inviting performers, actors, singers, movie professionals, stars and celebs of all cultural and pop-culture media to be a part of events to huge Telugu crowds regularly in the last 2 years. </p>
                            <p class="text-justify py-2"> A premier show of Sainikudu with a 4 city tour in the US was the first such premier in US for a south indian movie. The stars in the cast and crew including Mahesh, Namrata, Guna Sekhar and others toured and promoted it across the country to adoring Telugu NRIs. </p>
                        </div>
                    </div>
                    <!--/ events description -->
                </div>
            </section>
            <!--/ what we provide in social media management -->
            <!--/ summary 2-->
        </section>
        <!--/ sub page body -->
    </section>




    <!--/ main -->@endsection